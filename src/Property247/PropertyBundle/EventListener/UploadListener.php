<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 7/3/18
 * Time: 10:19 AM
 */

namespace Property247\PropertyBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Property247\MainBundle\Entity\Image;
use Property247\PropertyBundle\Entity\Property;
use Symfony\Component\HttpFoundation\File\File;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class UploadListener
 * @package Property247\PropertyBundle\EventListener
 * @DI\Service("property.imageupload.subscriber")
 * @DI\Tag("kernel.event_subscriber")
 */
class UploadListener implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $request;

    public static function getSubscribedEvents()
    {
        return [
            "oneup_uploader.post_persist" => "onUpload"
        ];
    }

    public function onUpload(PostPersistEvent $event)
    {
        /**
         * @var $file File
         */
        $propertyId = $this->request->getCurrentRequest()->get('propertyId');
        $property = $this->em->getRepository(Property::class)->find($propertyId);
        $file = $event->getFile();
        $response = $event->getResponse();
        $image = new Image();
        $image->setImageName($file->getFilename());

        if($property instanceof Property){
            $image->setProperty($property);
            $property->addImage($image);
            $this->em->persist($property);
        }

        $this->em->persist($image);
        $this->em->flush();

        $response['imageId'] = $image->getId();
        return $response;
    }

}