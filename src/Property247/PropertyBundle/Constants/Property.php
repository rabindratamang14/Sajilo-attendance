<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/19/18
 * Time: 12:40 AM
 */
namespace Property247\PropertyBundle\Constants;

class Property
{

    /**
     * property type
     */
    const PROPERTY_TYPE_HOUSE = 101;
    const PROPERTY_TYPE_LAND = 102;

    public static $propertyTypeArray = [
        self::PROPERTY_TYPE_HOUSE => 'House',
        self::PROPERTY_TYPE_LAND => 'Land'
    ];

    /**
     * land type constants
     */
    const LAND_TYPE_INDIVIDUAL = 201;
    const LAND_TYPE_PLOTTED = 202;
    const LAND_TYPE_AGRICULTURAL = 203;

    public static $landTypeArray = [
        self::LAND_TYPE_INDIVIDUAL => 'Individual',
        self::LAND_TYPE_PLOTTED => 'Plotted',
        self::LAND_TYPE_AGRICULTURAL => 'Agricultural'
    ];

    /**
     * house type constants
     */
    const HOUSE_TYPE_INDIVIDUAL = 301;
    const HOUSE_TYPE_RESIDENTIAL = 302;
    const HOUSE_TYPE_RENTAL = 303;
    const HOUSE_TYPE_BUNGLOW = 304;
    const HOUSE_TYPE_VILLA = 305;
    const HOUSE_TYPE_COLONY = 306;
    const HOUSE_TYPE_HOUSING = 307;
    const HOUSE_TYPE_APARTMENT = 308;

    public static $houseTypesArray = [
        self::HOUSE_TYPE_INDIVIDUAL => 'Individual',
        self::HOUSE_TYPE_RESIDENTIAL => 'Residential',
        self::HOUSE_TYPE_RENTAL => 'Rental',
        self::HOUSE_TYPE_BUNGLOW => 'Bungalow',
        self::HOUSE_TYPE_VILLA => 'Villa',
        self::HOUSE_TYPE_COLONY => 'Colony',
        self::HOUSE_TYPE_HOUSING => 'Housing',
        self::HOUSE_TYPE_APARTMENT => 'Apartment',
    ];

    /**
     * purposes
     */
    const PURPOSE_AGRICULTURAL = 401;
    const PURPOSE_RESIDENTIAL = 402;
    const SEMI_COMMERCIAL = 403;
    const PURPOSE_COMMERCIAL = 404;
    const PURPOSE_BOTH = '405';

    public static $purposesArray = [
        self::PURPOSE_AGRICULTURAL => 'Agricultural',
        self::PURPOSE_RESIDENTIAL => 'Residential',
        self::SEMI_COMMERCIAL => 'Semi Commercial',
        self::PURPOSE_COMMERCIAL => 'Commerical',
        self::PURPOSE_BOTH => 'Both (Residential and Commercial)',
    ];

    /**
     * road types
     */
    const ROAD_TYPE_PITCHED = 501;
    const ROAD_TYPE_GRAVELED = 502;
    const ROAD_TYPE_GORETO = 503;

    public static $roadTypeArray = [
        self::ROAD_TYPE_PITCHED => 'Pitched (Black topped)',
        self::ROAD_TYPE_GRAVELED => 'Graveled',
        self::ROAD_TYPE_GORETO => 'Goreto',
    ];


    /**
     * area measure type
     */
    const AREA_TYPE_BIGHA = 601;
    const AREA_TYPE_ROPANI = 602;
    const AREA_TYPE_KATTHA = 603;
    const AREA_TYPE_AANA = 604;
    const AREA_TYPE_DHUR = 605;
    const AREA_TYPE_PAISA = 606;
    const AREA_TYPE_DAAM = 607;

    public static $areaTypeArray = [
        self::AREA_TYPE_BIGHA => 'Bigha',
        self::AREA_TYPE_ROPANI => 'Ropani',
        self::AREA_TYPE_KATTHA => 'Kattha',
        self::AREA_TYPE_AANA => 'Aana',
        self::AREA_TYPE_DHUR => 'Dhur',
        self::AREA_TYPE_PAISA => 'Paisa',
        self::AREA_TYPE_DAAM => 'Daam'
    ];

    /**
     * distance measure type
     */
    const DISTANCE_TYPE_KM = 701;
    const DISTANCE_TYPE_METERS = 702;
    const DISTANCE_TYPE_FEET = 703;

    public static $distanceMeasureTypes = [
        self::DISTANCE_TYPE_KM => 'Kilometers',
        self::DISTANCE_TYPE_METERS => 'Meters',
        self::DISTANCE_TYPE_FEET => 'Feet',
    ];

    /**
     * area formats
     */
    const BIGHA_KATHA_DHUR_KARUWA = 801;
    const ROPANI_ANNA_PAISA_DAAM = 802;

    public static $areaFormats = [
        self::BIGHA_KATHA_DHUR_KARUWA => 'Bigha-Katha-Dhur-Karuwa',
        self::ROPANI_ANNA_PAISA_DAAM => 'Ropani-Anna-Paisa-Daam'
    ];

    /**
     * house structures
     */
    const RCC = 901;
    const MUDDED = 902;
    const OTHERS = 903;

    public static $houseStructureArray = [
        self::RCC => 'RCC',
        self::MUDDED => 'Mudded',
        self::OTHERS => 'Others'
    ];

    /**
     * directions constants
     */
    const NORTH = 'N';
    const NORTH_EAST = 'NE';
    const EAST = 'E';
    const EAST_SOUTH = 'ES';
    const SOUTH = 'S';
    const SOUTH_WEST = 'SW';
    const WEST = 'W';
    const NORTH_WEST = 'NW';

    public static $directionsArray = [
        self::NORTH => 'North',
        self::EAST => 'East',
        self::SOUTH => 'South',
        self::WEST => 'West',
        self::NORTH_EAST => 'North-East (NE)',
        self::NORTH_WEST => 'North-West (NW)',
        self::EAST_SOUTH => 'South-East (SE)',
        self::SOUTH_WEST => 'South-West (SW)'
    ];

    const FLOOR_AREA_TYPE_SQ_FT = '111';
    const FLOOR_AREA_TYPE_METER_SQUARE = '112';

    public static $floorAreaTypeArray = [
        self::FLOOR_AREA_TYPE_SQ_FT => 'Square Feet',
        self::FLOOR_AREA_TYPE_METER_SQUARE => 'Meter Square'
    ];

    const DOCUMENT_TYPE_LAND = '121';
    const DOCUMENT_TYPE_HOUSE = '122';
    const DOCUMENT_TYPE_BOTH = '123';

    public static $documentTypeArray = [
        self::DOCUMENT_TYPE_LAND => 'Land',
        self::DOCUMENT_TYPE_HOUSE => 'House',
        self::DOCUMENT_TYPE_BOTH => 'Both'
    ];

}