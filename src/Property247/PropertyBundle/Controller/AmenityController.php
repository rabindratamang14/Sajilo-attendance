<?php

namespace Property247\PropertyBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use Property247\MainBundle\Annotations\Permissions;
use Property247\PropertyBundle\Entity\Amenity;
use Property247\PropertyBundle\Form\AmenityType;
use Property247\PropertyBundle\Service\AmenityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class AmenityController
 * @package Property247\PropertyBundle\Controller
 * @Route("/admin")
 */
class AmenityController extends Controller
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @var AmenityService
     * @DI\Inject("property.amenity.service")
     */
    private $service;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    private $apy;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/amenity", name="admin_list_amenity")
     * @Permissions("list_amenity", group="amenity", desc="List Amenity")
     * @Breadcrumb("Amenity", routeName="admin_list_amenity")
     * @Breadcrumb("List")
     */
    public function listAction(Request $request){
        $filters = $request->query->all();
        $data['amenities'] = $this->service->getPaginatedAmenities($filters);
        return $this->render('@Property247Property/amenity/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/amenity/add", name="admin_amenity_add")
     * @Route("/amenity/{id}/update", name="admin_amenity_update")
     * @Permissions("add_amenity", group="amenity", desc="Add Amenity")
     * @Permissions("update_amenity", group="amenity", desc="Update Amenity")
     * @Breadcrumb("Amenity", routeName="admin_list_amenity")
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        if($id){
            $amenity = $this->em->getRepository(Amenity::class)->find($id);
            if(! $amenity instanceof Amenity){
                return $this->redirectToRoute('admin_add_amenity');
            }
            $this->apy->add($amenity->getName());
            $this->apy->add('Update');
            $message = "Amenity updated.";
            $data['isUpdating'] = true;
        }   else    {
            $this->apy->add('Add');
            $amenity = new Amenity();
            $message = "Amenity added.";
        }

        $form = $this->createForm(AmenityType::class, $amenity, $data);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /**
             * @var $formData Amenity
             */
            $formData = $form->getData();

            if(! $formData->getIconName()){
                $formData->setIconName($amenity->getIconName());
            }

            $this->em->persist($formData);

            try{
                $this->em->flush();
                $this->addFlash('success', $message);
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirectToRoute('admin_list_amenity');
        }
        $data['form'] = $form->createView();
        return $this->render('@Property247Property/amenity/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/amenity/{id}/delete", name="admin_delete_amenity")
     * @Permissions("delete_amenity", group="amenity", desc="Delete Amenity")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        if($id){
            $amenity = $this->em->getRepository(Amenity::class)->find($id);
            if($amenity instanceof Amenity){
                $amenity->setDeleted(true);
                $this->em->persist($amenity);
                try{
                    $this->em->flush();
                    $this->addFlash('success', 'Amenity deleted.');
                }   catch (\Throwable $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirectToRoute('admin_list_amenity');
    }

}
