<?php

namespace Property247\PropertyBundle\Controller;


use Dmishh\SettingsBundle\Manager\SettingsManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Annotations\Permissions;
use Property247\PropertyBundle\Entity\FloorPlan;
use Property247\PropertyBundle\Entity\FloorPlanAmenity;
use Property247\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Property247\PropertyBundle\Entity\Property;
use Property247\PropertyBundle\Form\PropertyType;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Property247\PropertyBundle\Service\PropertyService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Property247\MainBundle\Entity\Image;

/**
 * Class PropertyController
 * @package Property247\PropertyBundle\Controller
 */
class PropertyController extends Controller
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var PropertyService
     * @DI\Inject("property.service")
     */
    protected $service;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    protected $apy;

    /**
     * @var SettingsManagerInterface
     * @DI\Inject("settings_manager")
     */
    protected $settingService;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/property/land", name="list_land_property")
     * @Route("/agent/property/land", name="agent_list_land_property")
     * @Breadcrumb("Land",routeName="list_land_property")
     * @Breadcrumb("List")
     * @Permissions("list_land",group="property_land", route="list_land_property")
     */
    public function listLandAction(Request $request){
        $filters = $request->query->all();
        $filters['type'] = \Property247\PropertyBundle\Constants\Property::PROPERTY_TYPE_LAND;

        $user = $this->getUser();

        if(in_array('ROLE_AGENT', $user->getRoles()) or in_array('ROLE_AGENT_ADMIN', $user->getRoles())){
            $filters['agent'] = $user->getId();
        }

        $data['properties'] = $this->service->getPaginatedPropertyList($filters);
        $data['isLand'] = true;
        return $this->render('@Property247Property/property/list.html.twig',$data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/property/house", name="list_house_property")
     * @Route("/agent/property/house", name="agent_list_house_property")
     * @Breadcrumb("Housing",routeName="list_house_property")
     * @Permissions("list_house", group="property_house", route="list_house_property")
     * @Breadcrumb("List")
     */
    public function listHouseAction(Request $request){
        $filters = $request->query->all();
        $filters['type'] = \Property247\PropertyBundle\Constants\Property::PROPERTY_TYPE_HOUSE;

        $user = $this->getUser();

        if(in_array('ROLE_AGENT', $user->getRoles()) or in_array('ROLE_AGENT_ADMIN', $user->getRoles())){
            $filters['agent'] = $user->getId();
        }

        $data['properties'] = $this->service->getPaginatedPropertyList($filters);
        $filters['limit'] = 1;
        $data['isHouse'] = true;
        return $this->render('@Property247Property/property/list.html.twig',$data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/admin/property/house/add", name="add_house_property")
     * @Route("/admin/property/{agentId}/house/add", name="agent_add_house_property")
     * @Route("/admin/property/house/{id}/update", name="update_house_property")
     * @Route("/agent/property/house/{id}/update", name="agent_update_house_property")
     * @Breadcrumb("Housing", routeName="list_house_property")
     * @Permissions("add_house",group="property_house", route="add_house_property", parent="list_house")
     * @Permissions("update_house",group="property_house", route="update_house_property", parent="list_house")
     */
    public function createHouseAction(Request $request){
        $id = $request->get('id');
        $agentId = $request->get('agentId');
        $em = $this->getDoctrine()->getManager();
        $data['isUpdating'] = false;
        $data['isHouse'] = true;

        if($id){
            $data['isUpdating'] = true;
            $property = $em->getRepository(Property::class)->find($id);
            if(! $property instanceof Property){
                return $this->redirectToRoute('add_house_property');
            }
            $this->apy->add($property->getName());
            $this->apy->add($property->getPropertyId());
        }   else    {
            $this->apy->add('Add');
            $property = new Property();
        }

        if($agentId){
            $agent = $this->em->getRepository(User::class)->findOneBy([
                'id' => $agentId,
                'enabled' => true
            ]);

            if(
                $agent instanceof User &&
                (
                    in_array('ROLE_AGENT', $agent->getRoles()) or
                    in_array('ROLE_AGENT_ADMIN', $agent->getRoles())
                )
            ){
                $property->setAgent($agent);
            }
        }

        $user = $this->getUser();

        if(!$agentId and in_array('ROLE_AGENT', $user->getRoles()) or in_array('ROLE_AGENT_ADMIN', $user->getRoles())){
            $property->setAgent($user);
        }

        $property->setPropertyType(\Property247\PropertyBundle\Constants\Property::PROPERTY_TYPE_HOUSE);
        $form = $this->createForm(PropertyType::class, $property, [
            'user' => $this->getUser(),
            'isUpdating' => $data['isUpdating']
        ]);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){
            $imageIds = $request->get('imageIds');
            /**
             * @var $formData Property
             */
            $formData = $form->getData();
//            $areaUnit = $formData->getAreaUnit();
//            $totalArea = $formData->getTotalArea();
//            if($data['isUpdating'] == true && $areaUnit && $totalArea){
//                $totalAreaSqFt = $this->service->convertAreaToSquareFeet($totalArea, $areaUnit);
//                $formData->setTotalAreaInSqFt($totalAreaSqFt);
//            }

            $houseBuiltArea = $formData->getHouseBuildArea();
            $houseBuiltAreaUnit = $formData->getHouseBuildAreaUnit();
            if($data['isUpdating'] && $houseBuiltArea && $houseBuiltAreaUnit){
                $houseBuiltAreaSqFt = $this->service->convertAreaToSquareFeet($houseBuiltArea, $houseBuiltAreaUnit);
                $formData->setHouseBuildAreaInSqFt($houseBuiltAreaSqFt);
            }

            $userRoles = $this->getUser()->getRoles();
            if(in_array('ROLE_SUPER_ADMIN', $userRoles)){
                $formData->setApproved(true);
            }

            $floorPlans = $formData->getFloorPlannings();
            /**
             * @var $f FloorPlan
             */
            if($floorPlans){
                foreach ($floorPlans as $f){
                    $f->setProperty($formData);
                    $floorPlanAmenities = $f->getFloorPlanAmenities();
                    /**
                     * @var $a FloorPlanAmenity
                     */
                    foreach ($floorPlanAmenities as $a){
                        $a->setFloorPlan($f);
                    }
                }
            }
            $em->persist($formData);

            try{
                $em->flush();
                $this->service->savePropertyId($formData);
                $this->service->addImages($imageIds,$formData);
                $message = $data['isUpdating'] ? 'Property updated.' : 'New Property added.';
                $this->addFlash('success',$message);
                $route = $request->get('_route');
                if($route == 'agent_add_house_property' or $route == 'agent_house_land_property'){
                    $routeName = 'agent_list_house_property';
                }   else    {
                    $routeName = 'list_house_property';
                }
                if(! $data['isUpdating']){
                    return $this->redirectToRoute($routeName);
                }
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }

        $data['form'] = $form->createView();
        return $this->render('@Property247Property/property/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/admin/property/land/add", name="add_land_property")
     * @Route("/admin/property/{agentId}/land/add", name="agent_add_land_property")
     * @Route("/admin/property/land/{id}/update", name="update_land_property")
     * @Route("/agent/property/land/{id}/update", name="agent_update_land_property")
     * @Breadcrumb("Land", routeName="list_land_property")
     * @Permissions("add_land",group="property_land", route="update_land_property", parent="list_land")
     * @Permissions("update_land",group="property_land", route="update_land_property", parent="list_land")
     */
    public function createLandAction(Request $request){
        $id = $request->get('id');
        $agentId = $request->get('agentId');
        $em = $this->getDoctrine()->getManager();
        $data['isUpdating'] = false;
        $data['isLand'] = true;

        if($id){
            $data['isUpdating'] = true;
            $property = $em->getRepository(Property::class)->find($id);
            if(! $property instanceof Property){
                return $this->redirectToRoute('add_property');
            }
            $this->apy->add('Update');
        }   else    {
            $property = new Property();
            $this->apy->add('Add');
        }

        if($agentId){
            $agent = $this->em->getRepository(User::class)->findOneBy([
                'id' => $agentId,
                'enabled' => true
            ]);

            if(
                $agent instanceof User &&
                (
                    in_array('ROLE_AGENT', $agent->getRoles()) or
                    in_array('ROLE_AGENT_ADMIN', $agent->getRoles())
                )
            ){
                $property->setAgent($agent);
            }
        }


        $user = $this->getUser();

        if(!$agentId and in_array('ROLE_AGENT', $user->getRoles()) or in_array('ROLE_AGENT_ADMIN', $user->getRoles())){
            $property->setAgent($user);
        }

        $propertyType = \Property247\PropertyBundle\Constants\Property::PROPERTY_TYPE_LAND;
        $property->setPropertyType($propertyType);
        $form = $this->createForm(PropertyType::class, $property, [
            'propertyType' => $propertyType,
            'user' => $this->getUser(),
            'isUpdating' => $data['isUpdating']
        ]);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){

            $imageIds = $request->get('imageIds');

            /**
             * @var $formData Property
             */
            $formData = $form->getData();
//            $areaUnit = $formData->getAreaUnit();
//            $totalArea = $formData->getTotalArea();
//            if($data['isUpdating'] == true && $areaUnit && $totalArea){
//                $totalAreaSqFt = $this->service->convertAreaToSquareFeet($totalArea, $areaUnit);
//                $formData->setTotalAreaInSqFt($totalAreaSqFt);
//            }
            $em->persist($formData);

            try{
                $em->flush();
                $this->service->addImages($imageIds,$formData);
                $this->service->savePropertyId($formData);
                $message = $data['isUpdating'] ? 'Property updated.' : 'New Property added.';
                $this->addFlash('success',$message);
                $route = $request->get('_route');
                if($route == 'agent_add_land_property' or $route == 'agent_update_land_property'){
                    $routeName = 'agent_list_land_property';
                }   else    {
                    $routeName = 'list_land_property';
                }
                if(! $data['isUpdating']){
                    return $this->redirectToRoute($routeName);
                }
            }   catch (\Throwable $e){
                $this->addFlash('error', $e);
            }
        }

        $data['form'] = $form->createView();
        return $this->render('@Property247Property/property/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/property/{id}/delete", name="delete_property")
     * @Route("/agent/property/{id}/delete", name="agent_delete_property")
     * @Permissions("delete_house",group="property_house", route="delete_property", parent="list_house")
     * @Permissions("delete_land",group="property_land", route="agent_delete_property", parent="list_land")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        if($id){
            $property = $this->service->getPropertyById($id);
            if($property instanceof Property){
                $property->setDeleted(true);
                $this->em->persist($property);
                try{
                    $this->em->flush();
                    $this->addFlash('success', 'One property deleted.');
                }   catch (\Throwable $e){
                    $this->addFlash('error', $e->getMessage());
                }

                $roles = $this->getUser()->getRoles();
                $route = '';
                if(in_array('ROLE_AGENT', $roles) or in_array('ROLE_AGENT_ADMIN', $roles)){
                    $route = 'agent_';
                }
                $route .= $property->getPropertyType() == \Property247\PropertyBundle\Constants\Property::PROPERTY_TYPE_LAND ? 'list_land_property' : 'list_house_property';
                return $this->redirectToRoute($route);
            }
        }
        return $this->redirectToRoute('property_dashboard');
    }

}
