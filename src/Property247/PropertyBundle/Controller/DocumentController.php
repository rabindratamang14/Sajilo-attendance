<?php

namespace Property247\PropertyBundle\Controller;

use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Property247\PropertyBundle\Form\DocumentType;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Property247\PropertyBundle\Entity\Document;
use Property247\MainBundle\Annotations\Permissions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DocumentController extends Controller
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    public $apy;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/documents", name="admin_list_document")
     * @Permissions("list_document", group="document", desc="List Documents")
     */
    public function indexAction(Request $request){
        $filters = $request->query->all();
        $data['form'] = $this->createForm(DocumentType::class,new Document())->createView();
        $data['documents'] = $this->em->getRepository(Document::class)->findBy(['deleted' => false]);
        return $this->render('@Property247Property/document/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/document/add", name="admin_add_document")
     * @Route("/admin/document/{id}/update", name="admin_update_document")
     * @Permissions("add_document", group="document", desc="Add Document")
     * @Permissions("update_document", group="document", desc="Update document")
     */
    public function createAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        if($id){
            $document = $this->em->getRepository(Document::class)->find($id);
            if(! $document instanceof Document){
                return $this->redirectToRoute('admin_list_document');
            }
            $data['isUpdating'] = true;
            $message = "Document updated.";
        }   else    {
            $document = new Document();
            $message = "Document added.";
        }
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);
        if($form->isSubmitted() and $form->isValid()){
            $formData = $form->getData();
            $this->em->persist($formData);

            try{
                $this->em->flush();
                $this->addFlash('success', $message);
                return $this->redirectToRoute('admin_list_document');
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }
        $data['form'] = $form->createView();
        $data['documents'] = $this->em->getRepository(Document::class)->findBy(['deleted' => false]);
        return $this->render('@Property247Property/document/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/document/{id}/delete", name="admin_delete_document")
     * @Permissions("delete_document", group="document", desc="Delete document")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        $document = $this->em->getRepository(Document::class)->find($id);
        if($document instanceof Document){
            $document->setDeleted(true);
            $this->em->persist($document);
            try{
                $this->em->flush();
                $this->addFlash('success', 'Document deleted.');
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->redirectToRoute('admin_list_document');
    }

}
