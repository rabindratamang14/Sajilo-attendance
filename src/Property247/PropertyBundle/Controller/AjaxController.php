<?php

namespace Property247\PropertyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Property247\MainBundle\Entity\Image;
use Property247\MainBundle\Form\ImageType;
use Property247\PropertyBundle\Entity\Property;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AjaxController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/admin/ajax/{id}/toogleStatus", name="admin_ajax_toogle_property_status")
     */
    public function toogleApprovePropertyAction(Request $request){
        $id = $request->get('id');
        if($id){
            $property = $this->em->getRepository(Property::class)->findOneBy([
                'id' => $id,
                'deleted' => false
            ]);
            if($property instanceof Property){
                if($property->isApproved()){
                    $property->setApproved(false);
                }   else    {
                    $property->setApproved(true);
                }
                $this->em->persist($property);

                try{
                    $this->em->flush();
                    return new JsonResponse([
                        'success' => true,
                        'message' => 'Property updated.'
                    ]);
                }   catch (\Throwable $e){
                    return new JsonResponse([
                        'success' => false,
                        'message' => $e->getMessage()
                    ]);
                }
            }   else    {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Property does not exist'
                ]);
            }
        }

        return new JsonResponse([
            'success' => false,
            'message' => 'Something went wrong.'
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/admin/ajax/image/{id}/render", name="admin_ajax_render_image_view")
     */
    public function renderImage(Request $request){
        $id = $request->get('id');
        if($id){
            $image = $this->getDoctrine()->getManager()->getRepository(Image::class)->find($id);
            if($image instanceof Image){
                $template = $this->renderView('@Property247Property/partials/imageList.html.twig',
                    ['image' => $image]
                );
                return new JsonResponse([
                    'success' => true,
                    'template' => $template
                ]);
            }
        }
        return new JsonResponse([
            'success' => false,
            'template' => ''
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/admin/ajax/{propertyId}/renderImages", name="admin_ajax_render_all_property_images")
     */
    public function renderAllPropertyImages(Request $request){
        $propertyId = $request->get('propertyId');
        $property = $this->getDoctrine()->getManager()->getRepository(Property::class)->find($propertyId);
        if($property instanceof Property){
            $data['images'] = $property->getImages();
            $response['template'] = $this->renderView('@Property247Property/partials/allImages.html.twig', $data);
            $response['success'] = true;
            return new JsonResponse($response);
        }
        return new JsonResponse([
            'success' => true,
            'template' => ''
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/admin/ajax/{propertyId}/{imageId}/setFeaturedImage", name="admin_ajax_set_property_featured_image")
     */
    public function setFeaturedPropertyImage(Request $request){
        $propertyId = $request->get('propertyId');
        $imageId = $request->get('imageId');
        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository(Property::class)->find($propertyId);
        $image = $em->getRepository(Image::class)->find($imageId);

        if(! $property instanceof Property or ! $image instanceof Image){
            return new JsonResponse([
                'success' => false,
                'message' => 'property or image not found.'
            ]);
        }

        $images = $property->getImages();

        /**
         * @var $i Image
         */
        foreach ($images as $i){
            $i->setFeatured(false);
        }

        $image->setFeatured(true);
        $em->persist($image);

        try{
            $em->flush();
            return new JsonResponse([
                'success' => true,
                'message' => 'Featured image set.'
            ]);
        } catch (\Throwable $e){
            return new JsonResponse([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/admin/ajax/{imageId}/update", name="admin_ajax_update_image")
     */
    public function handleImageFormRequestAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $imageId = $request->get('imageId');
        $image = $em->getRepository(Image::class)->find($imageId);
        $form = $this->createForm(ImageType::class, $image);
        $form->handleRequest($request);
        if($form->isSubmitted() and $form->isValid()){
            $formData = $form->getData();
            $em->persist($formData);
            try{
                $em->flush();
                return new JsonResponse([
                    'success' => true,
                    'message' => 'Image updated.'
                ]);
            }   catch (\Throwable $e){
                return new JsonResponse([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }
        }
        $data['imageId'] = $imageId;
        $data['form'] = $form->createView();
        $response['success'] = true;
        $response['template'] = $this->renderView('@Property247Main/Image/form.html.twig', $data);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/admin/ajax/{propertyId}/{imageId}/deleteImage", name="admin_ajax_delete_property_image")
     */
    public function deletePropertyImage(Request $request){
        $propertyId = $request->get('propertyId');
        $imageId = $request->get('imageId');
        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository(Property::class)->find($propertyId);
        $image = $em->getRepository(Image::class)->find($imageId);

        if(! $property instanceof Property or ! $image instanceof Image){
            return new JsonResponse([
                'success' => false,
                'message' => 'property or image not found.'
            ]);
        }

        $em->remove($image);
        try{
            $em->flush();
            return new JsonResponse([
                'success' => true,
                'message' => 'Property image deleted.'
            ]);
        }   catch (\Throwable $e){
            return new JsonResponse([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }


    }

}
