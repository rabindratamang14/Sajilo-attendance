<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/21/18
 * Time: 11:07 AM
 */

namespace Property247\PropertyBundle\Twig;

use Property247\MainBundle\Entity\Image;
use Twig\Extension\AbstractExtension;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManagerInterface;
use Property247\PropertyBundle\Entity\Property;
use Property247\PropertyBundle\Service\PropertyService;
use YarshaStudio\LocalityBundle\Entity\City;
use YarshaStudio\LocalityBundle\Entity\District;
use YarshaStudio\LocalityBundle\Entity\Province;
use Property247\PropertyBundle\Constants\Property as PropertyConstants;

/**
 * Class PropertyExtension
 * @package Property247\PropertyBundle\Twig
 * @DI\Service("property.extension", public=true)
 * @DI\Tag("twig.extension")
 */
class PropertyExtension extends AbstractExtension
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var PropertyService
     * @DI\Inject("property.service")
     */
    public $propertyService;

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('convert_to_sqft', [$this, 'convertToSqFt']),
            new \Twig_SimpleFunction('count_property', [$this, 'countProperty']),
            new \Twig_SimpleFunction('render_provinces', [$this, 'renderProvinces'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('render_districts', [$this, 'renderDistricts'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('render_city_types', [$this, 'renderCityTypes'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('render_cities', [$this, 'renderCities'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('get_property_featured_image', [$this, 'getPropertyFeaturedImage']),
        ];
    }

    public function getFilters(){
        return [
            new \Twig_SimpleFilter('propertyTypeName', array($this, 'getPropertyTypeName')),
            new \Twig_SimpleFilter('purposeTypeName', array($this, 'getPurposeTypeName')),
            new \Twig_SimpleFilter('documentTypeName', array($this, 'getDocumentTypeName')),
            new \Twig_SimpleFilter('getDistanceMeasureName', array($this, 'getDistanceMeasureName')),
            new \Twig_SimpleFilter('getTotalAreaUnit', array($this, 'getTotalAreaUnit')),
        ];
    }

    public function getPropertyFeaturedImage(Property $property){
        $image = $this->em->getRepository(Image::class)->findOneBy([
            'property' => $property,
            'featured' => true
        ]);
        if(! $image instanceof Image){
            $image = $this->em->getRepository(Image::class)->findOneBy([
                'property' => $property
            ]);
        }
        return $image;
    }

    public function convertToSqFt($area, $type){
        return $this->propertyService->convertAreaToSquareFeet($area,$type);
    }

    public function getPropertyTypeName(Property $property){
        $landTypeArray = PropertyConstants::$landTypeArray;
        $houseTypeArray = PropertyConstants::$houseTypesArray;
        $typeName = '';
        $type = $property->getType();
        if($property->getPropertyType() == PropertyConstants::PROPERTY_TYPE_HOUSE){
            if(array_key_exists($type,$houseTypeArray)){
                $typeName = $houseTypeArray[$type];
            }
        }   else    {
            if(array_key_exists($type,$landTypeArray)){
                $typeName = $landTypeArray[$type];
            }
        }
        return $typeName;
    }

    public function getPurposeTypeName($type){
        $purposeArray = PropertyConstants::$purposesArray;
        $purposeName = '';
        if($type == PropertyConstants::PROPERTY_TYPE_HOUSE){
            if(array_key_exists($type,$purposeArray)){
                $purposeName = $purposeArray[$purposeArray];
            }
        }   else    {
            if(array_key_exists($type,$purposeArray)){
                $purposeName = $purposeArray[$type];
            }
        }
        return $purposeName;
    }

    public function getTotalAreaUnit($type){
        $areaTypeArray = PropertyConstants::$areaFormats;
        if(array_key_exists($type, $areaTypeArray)){
            return $areaTypeArray[$type];
        }
        return '';
    }

    public function getDocumentTypeName($type){
        $documentTypeArray = PropertyConstants::$documentTypeArray;
        if(array_key_exists($type, $documentTypeArray)){
            return $documentTypeArray[$type];
        }
        return '';
    }

    public function getDistanceMeasureName($type){
        $distanceMeasureTypeArray = PropertyConstants::$distanceMeasureTypes;
        if(array_key_exists($type, $distanceMeasureTypeArray)){
            return $distanceMeasureTypeArray[$type];
        }
        return '';
    }

    public function countProperty($type, $filters = []){
        $type = $type == 'h' ? PropertyConstants::PROPERTY_TYPE_HOUSE : PropertyConstants::PROPERTY_TYPE_LAND;
        return $this->propertyService->countProperty($type, $filters);
    }

    public function renderProvinces($class = 'select-province', $id='select-province'){
        $provinces = $this->em->getRepository(Province::class)->findAll();
        $html = "<div class='form-group'><label for='{$id}' class='col-md-4'>Select Province</label>";
        $html .= "<div class='col-md-8'><select id='select-province' name='{$class}' class='form-control {$class}'>";
        $html .= "<option value=''>Select provice</option>";

        /**
         * @var $p Province
         */
        foreach ($provinces as $p){
            $html .= "<option value='{$p->getId()}'>{$p->getProvinceName()}</option>";
        }

        $html .= "</select></div></div>";
        return $html;
    }

    public function renderDistricts($class = 'select-district', $id='select-district'){
        $districts = $this->em->getRepository(District::class)->findAll();
        $html = "<div class='form-group'><label for='{$id}' class='col-md-4'>Select District</label>";
        $html .= "<div class='col-md-8'><select name='select-district' id='{$id}' class='form-control {$class}'>";
        $html .= "<option value=''>Select District</option>";

        /***
         * @var $d District
         */
        foreach ($districts as $d){
            $provinceId = $d->getProvince() ? $d->getProvince()->getId() : 0;
            $html .= "<option value='{$d->getId()}' class='select-district-option' data-province-id='{$provinceId}'>{$d->getNameEn()}</option>";
        }
        $html .= "</select></div></div>";
        return $html;
    }

    public function renderCityTypes($class = 'select-city-type', $id = 'select-city-type'){
        $cityTypes = City::$typeArray;
        $html = "<label for='{$id}'>Select City Type</label><br/>";
        foreach ($cityTypes as $k => $v){
            $html .= "<input type='radio' class='{$class}' name='city-type' value='$k'>&nbsp; $v &nbsp;";
        }
        return $html;
    }

    public function renderCities($class = 'select-city',$id='select-city'){
        $cities = $this->em->getRepository(City::class)->findBy([
            'deleted' => false
        ]);
        $html = "<label for='{$id}'>Select City</label>";
        $html .= "<select name='form-control {$class}' class='{$class}'>";
        $html .= "<option value=''>Select City</option>";

        /**
         * @var $c City
         */
        foreach ($cities as $c){
            $districtId = $c->getDistrict() ? $c->getDistrict()->getId() : 0;
            $html .= "<option value='{$c->getId()}' data-district-id='{$districtId}'>{$c->getName()}</option>";
        }

        $html .= "</select>";
        return $html;
    }

}