<?php

namespace Property247\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Amenity
 *
 * @ORM\Table(name="amenity")
 * @ORM\Entity(repositoryClass="Property247\PropertyBundle\Repository\AmenityRepository")
 * @Vich\Uploadable
 */
class Amenity
{

    const AMENITY_TYPE_PUBLIC = 'public';
    const AMENITY_TYPE_PRIVATE = 'private';
    const AMENITY_TYPE_INTERNAL = 'internal';

    public static $typeArray = [
        self::AMENITY_TYPE_PUBLIC => 'Public',
        self::AMENITY_TYPE_PRIVATE => 'Private',
        self::AMENITY_TYPE_INTERNAL => 'Floor Plan Configuration'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/*"})
     * @Vich\UploadableField(mapping="amenity_icon", fileNameProperty="iconName")
     */
    private $iconImage;

    /**
     * @var string
     * @ORM\Column(name="icon_name", type="string", nullable=true)
     */
    private $iconName;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="Property247\PropertyBundle\Entity\FloorPlan", inversedBy="amenityType")
     */
    private $floorPlanning;

    use CreatedUpdatedOnTrait;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return File
     */
    public function getIconImage()
    {
        return $this->iconImage;
    }

    /**
     * @param File $iconImage
     */
    public function setIconImage(File $iconImage)
    {
        $this->iconImage = $iconImage;
    }

    /**
     * @return string
     */
    public function getIconName()
    {
        return $this->iconName;
    }

    /**
     * @param string $iconName
     */
    public function setIconName($iconName)
    {
        $this->iconName = $iconName;
    }

    /**
     * @return mixed
     */
    public function getFloorPlanning()
    {
        return $this->floorPlanning;
    }

    /**
     * @param mixed $floorPlanning
     */
    public function setFloorPlanning($floorPlanning)
    {
        $this->floorPlanning = $floorPlanning;
    }

    public function __toString()
    {
        return $this->name ? $this->name : 'amenity';
    }

}