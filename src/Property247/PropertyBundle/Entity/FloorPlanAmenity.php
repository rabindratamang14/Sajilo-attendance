<?php

namespace Property247\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FloorPlanAmenity
 *
 * @ORM\Table(name="floor_plan_amenity")
 * @ORM\Entity(repositoryClass="Property247\PropertyBundle\Repository\FloorPlanAmenityRepository")
 */
class FloorPlanAmenity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Amenity
     * @ORM\ManyToOne(targetEntity="Property247\PropertyBundle\Entity\Amenity")
     */
    private $amenity;

    /**
     * @var integer
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var FloorPlan
     * @ORM\ManyToOne(targetEntity="Property247\PropertyBundle\Entity\FloorPlan", inversedBy="floorPlanAmenity", cascade={"persist"})
     */
    private $floorPlan;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Amenity
     */
    public function getAmenity()
    {
        return $this->amenity;
    }

    /**
     * @param Amenity $amenity
     */
    public function setAmenity(Amenity $amenity)
    {
        $this->amenity = $amenity;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return FloorPlan
     */
    public function getFloorPlan()
    {
        return $this->floorPlan;
    }

    /**
     * @param FloorPlan $floorPlan
     */
    public function setFloorPlan(FloorPlan $floorPlan)
    {
        $this->floorPlan = $floorPlan;
    }

}

