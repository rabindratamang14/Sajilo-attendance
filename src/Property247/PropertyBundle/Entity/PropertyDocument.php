<?php

namespace Property247\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * PropertyDocument
 *
 * @ORM\Table(name="property_document")
 * @ORM\Entity(repositoryClass="Property247\PropertyBundle\Repository\PropertyDocumentRepository")
 * @Vich\Uploadable()
 */
class PropertyDocument
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Document
     * @ORM\ManyToOne(targetEntity="Property247\PropertyBundle\Entity\Document")
     */
    private $document;

    /**
     * @var File
     * @Vich\UploadableField(mapping="property_documents", fileNameProperty="documentFileName")
     */
    private $documentFile;

    /**
     * @var static
     * @ORM\Column(name="document_file_name", type="string", nullable=true)
     */
    private $documentFileName;

    /**
     * @var Property
     * @ORM\ManyToOne(targetEntity="Property247\PropertyBundle\Entity\Property", inversedBy="documents", cascade={"persist"})
     */
    private $property;

    use CreatedUpdatedOnTrait;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param Document $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return File
     */
    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    /**
     * @param File $documentFile
     */
    public function setDocumentFile($documentFile)
    {
        $this->documentFile = $documentFile;
    }

    /**
     * @return static
     */
    public function getDocumentFileName()
    {
        return $this->documentFileName;
    }

    /**
     * @param static $documentFileName
     */
    public function setDocumentFileName($documentFileName)
    {
        $this->documentFileName = $documentFileName;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }

}
