<?php

namespace Property247\PropertyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FloorPlan
 *
 * @ORM\Table(name="floor_plan")
 * @ORM\Entity(repositoryClass="Property247\PropertyBundle\Repository\FloorPlanRepository")
 * @Vich\Uploadable
 */
class FloorPlan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="floor_name", type="string")
     */
    private $floorName;

    /**
     * @var string
     * @ORM\Column(name="total_area", type="string", nullable=true)
     */
    private $totalArea;

    /**
     * @var string
     * @ORM\Column(name="area_unit", type="string", nullable=true)
     */
    private $areaUnit;

    /**
     * @var string
     * @ORM\Column(name="configuration", type="string", nullable=true)
     */
    private $configuration;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/*"})
     * @Vich\UploadableField(mapping="floorplan_image", fileNameProperty="image2DName")
     */
    private $image2DFile;

    /**
     * @var string
     * @ORM\Column(name="image2dname", type="string", nullable=true)
     */
    private $image2DName;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/*"})
     * @Vich\UploadableField(mapping="floorplan_image", fileNameProperty="image3DName")
     */
    private $image3DFile;

    /**
     * @var string
     * @ORM\Column(name="image3dname", type="string", nullable=true)
     */
    private $image3DName;

    /**
     * @var Amenity
     * @ORM\OneToMany(targetEntity="Property247\PropertyBundle\Entity\FloorPlanAmenity", mappedBy="floorPlan", orphanRemoval=true, cascade={"persist"})
     */
    private $floorPlanAmenities;

    /**
     * @var integer
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity = 0;

    /**
     * @var Property
     * @ORM\ManyToOne(targetEntity="Property247\PropertyBundle\Entity\Property", inversedBy="floorPlanning", cascade={"persist"})
     */
    private $property;


    use CreatedUpdatedOnTrait;

    public function __construct()
    {
        $this->floorPlanAmenities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFloorName()
    {
        return $this->floorName;
    }

    /**
     * @param string $floorName
     */
    public function setFloorName($floorName)
    {
        $this->floorName = $floorName;
    }

    /**
     * @return Amenity
     */
    public function getFloorPlanAmenities()
    {
        return $this->floorPlanAmenities;
    }

    /**
     * @param Amenity $floorPlanAmenity
     */
    public function setFloorPlanAmenities($floorPlanAmenity)
    {
        $this->floorPlanAmenities = $floorPlanAmenity;
    }

    /**
     * @param FloorPlanAmenity $floorPlanAmenity
     */
    public function addFloorPlanAmenity(FloorPlanAmenity $floorPlanAmenity){
        $this->floorPlanAmenities->add($floorPlanAmenity);
    }

    /**
     * @param FloorPlanAmenity $floorPlanAmenity
     */
    public function removeFloorPlanAmenity(FloorPlanAmenity $floorPlanAmenity){
        if($this->floorPlanAmenities->contains($floorPlanAmenity)){
            $this->floorPlanAmenities->removeElement($floorPlanAmenity);
        }
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getTotalArea()
    {
        return $this->totalArea;
    }

    /**
     * @param string $totalArea
     */
    public function setTotalArea($totalArea)
    {
        $this->totalArea = $totalArea;
    }

    /**
     * @return string
     */
    public function getAreaUnit()
    {
        return $this->areaUnit;
    }

    /**
     * @param string $areaUnit
     */
    public function setAreaUnit($areaUnit)
    {
        $this->areaUnit = $areaUnit;
    }

    /**
     * @return string
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param string $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return File
     */
    public function getImage2DFile()
    {
        return $this->image2DFile;
    }

    /**
     * @param File $image2DFile
     */
    public function setImage2DFile($image2DFile)
    {
        $this->image2DFile = $image2DFile;
    }

    /**
     * @return string
     */
    public function getImage2DName()
    {
        return $this->image2DName;
    }

    /**
     * @param string $image2DName
     */
    public function setImage2DName($image2DName)
    {
        $this->image2DName = $image2DName;
    }

    /**
     * @return File
     */
    public function getImage3DFile()
    {
        return $this->image3DFile;
    }

    /**
     * @param File $image3DFile
     */
    public function setImage3DFile($image3DFile)
    {
        $this->image3DFile = $image3DFile;
    }

    /**
     * @return string
     */
    public function getImage3DName()
    {
        return $this->image3DName;
    }

    /**
     * @param string $image3DName
     */
    public function setImage3DName($image3DName)
    {
        $this->image3DName = $image3DName;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }

    public function __toString()
    {
        return $this->floorName ? $this->floorName : 'Floor plan';
    }

}

