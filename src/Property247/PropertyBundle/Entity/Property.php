<?php

namespace Property247\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\UserBundle\Entity\User;
use Property247\MainBundle\Entity\Image;
use Doctrine\Common\Collections\ArrayCollection;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Symfony\Component\HttpFoundation\File\File;
use YarshaStudio\LocalityBundle\Entity\City;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Property
 *
 * @ORM\Table(name="property")
 * @ORM\Entity(repositoryClass="Property247\PropertyBundle\Repository\PropertyRepository")
 * @Vich\Uploadable()
 */
class Property
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", unique=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(name="property_type", type="string")
     */
    private $propertyType;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="suitable_for", type="string")
     */
    private $suitableFor;

    /**
     * @var string
     * @ORM\Column(name="property_id", type="string", nullable=true)
     */
    private $propertyId;

    /**
     * @var string
     * @ORM\Column(name="price", type="string")
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="road_access", type="string", nullable=true)
     */
    private $roadAccess;

    /**
     * @var string
     * @ORM\Column(name="road_access_unit", type="string", nullable=true)
     */
    private $roadAccessUnit;

    /**
     * @var string
     * @ORM\Column(name="built_date", type="string", nullable=true)
     */
    private $builtDate;

    /**
     * @var string
     * @ORM\Column(name="total_area", type="string", nullable=true)
     */
    private $totalArea;

    /**
     * @var string
     * @ORM\Column(name="area_unit", type="string", nullable=true)
     */
    private $areaUnit;

    /**
     * @var integer
     * @ORM\Column(name="total_area_in_sq_ft", type="integer", nullable=true)
     */
    private $totalAreaInSqFt;

    /**
     * @var string
     * @ORM\Column(name="location", type="string", nullable=true)
     */
    private $location;

    /**
     * @var City
     * @ORM\ManyToOne(targetEntity="YarshaStudio\LocalityBundle\Entity\City")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="latitude", type="string", nullable=true)
     */
    private $latitude;

    /**
     * @var string
     * @ORM\Column(name="longitude",type="string", nullable=true)
     */
    private $longitude;

    /**
     * @var string
     * @ORM\Column(name="house_structure", type="string", nullable=true)
     */
    private $houseStructure;

    /**
     * @var string
     * @ORM\Column(name="house_structure_description", type="string", nullable=true)
     */
    private $houseStructureDescription;

    /**
     * @var integer
     * @ORM\Column(name="house_build_area", type="integer", nullable=true)
     */
    private $houseBuildArea;

    /**
     * @var string
     * @ORM\Column(name="house_build_area_unit", type="string", nullable=true)
     */
    private $houseBuildAreaUnit;

    /**
     * @var integer
     * @ORM\Column(name="house_build_area_in_sq_ft", type="integer", nullable=true)
     */
    private $houseBuildAreaInSqFt;

    /**
     * @var string
     * @ORM\Column(name="total_floor_coverage", type="string", nullable=true)
     */
    private $totalFloorCoverage;

    /**
     * @var string
     * @ORM\Column(name="floor_coverage_area_unit", type="string", nullable=true)
     */
    private $floorCoverageAreaUnit;

    /**
     * @var string
     * @ORM\Column(name="total_floor_coverage_area_unit", type="string", nullable=true)
     */
    private $totalFloorCoverageAreaUnit;

    /**
     * @var string
     * @ORM\Column(name="house_faced_on", type="string", nullable=true)
     */
    private $houseFacedOn;

    /**
     * @var string
     * @ORM\Column(name="front_face", type="string", nullable=true)
     */
    private $frontFace;

    /**
     * @var string
     * @ORM\Column(name="front_face_unit", type="string", nullable=true)
     */
    private $frontFaceUnit;

    /**
     * @var string
     * @ORM\Column(name="back_face", type="string", nullable=true)
     */
    private $backFace;

    /**
     * @var string
     * @ORM\Column(name="back_face_unit", type="string", nullable=true)
     */
    private $backFaceUnit;

    /**
     * @var string
     * @ORM\Column(name="road_type", type="string", nullable=true)
     */
    private $roadType;

    /**
     * @var string
     * @ORM\ManyToMany(targetEntity="Property247\PropertyBundle\Entity\Amenity", cascade={"persist"})
     * @ORM\JoinTable(name="property_amenity_relation",
     *      joinColumns={ @ORM\JoinColumn(name="property_id", referencedColumnName="id")},
     *      inverseJoinColumns={ @ORM\JoinColumn(name="amenity_id", referencedColumnName="id")}
     *)
     */
    private $privateAmenities;

    /**
     * @var string
     * @ORM\ManyToMany(targetEntity="Property247\PropertyBundle\Entity\Amenity", cascade={"persist"})
     */
    private $publicAmenities;

    /**
     * @var integer
     * @ORM\Column(name="distance_from_main_road", type="integer", nullable=true)
     */
    private $distanceFromMainRoad;

    /**
     * @var string
     * @ORM\Column(name="main_road_name", type="string", nullable=true)
     */
    private $mainRoadName;

    /**
     * @var string
     * @ORM\Column(name="distance_measure_type", type="string", nullable=true)
     */
    private $distanceMeasureType;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Property247\PropertyBundle\Entity\FloorPlan", cascade={"persist"}, mappedBy="property", orphanRemoval=true)
     */
    private $floorPlannings;

    /**
     * @var string
     * @ORM\Column(name="kitta_number", type="string", nullable=true)
     */
    private $kittaNumber;

    /**
     * @var string
     * @ORM\Column(name="ward_number_on_map", type="string", nullable=true)
     */
    private $wardNumberOnMap;

    /**
     * @var string
     * @ORM\Column(name="street_name", type="string", nullable=true)
     */
    private $streetName;

    /**
     * @var string
     * @ORM\Column(name="tole_ward_number", type="string", nullable=true)
     */
    private $toleWardNumber;

    /**
     * @var boolean
     * @ORM\Column(name="furnished", type="boolean", nullable=true)
     */
    private $furnished;

    /**
     * @var integer
     * @ORM\Column(name="top_water_tank", type="integer", nullable=true)
     */
    private $topWaterTank;

    /**
     * @var integer
     * @ORM\Column(name="reserve_water_tank", type="integer", nullable=true)
     */
    private $reserveWaterTank;

    /**
     * @var string
     * @ORM\Column(name="storeyed", type="string", nullable=true)
     */
    private $storeyed;

//    /**
//     * @var SocialMediaSetting
//     * @ORM\OneToOne(targetEntity="Property247\SettingBundle\Entity\SocialMediaSetting", mappedBy="property", cascade={"persist"})
//     */
//    private $socialMediaSetting;

    /**
     * @var boolean
     * @ORM\Column(name="socail_media_promotion", type="boolean")
     */
    private $socailMediaPromotion = 0;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Property247\PropertyBundle\Entity\PropertyDocument", cascade={"persist"}, mappedBy="property")
     */
    private $documents;

    /**
     * @var bool
     * @ORM\Column(name="approved", type="boolean")
     */
    private $approved = false;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Property247\UserBundle\Entity\User")
     */
    private $agent;

    /**
     * @var int
     * @ORM\Column(name="total_views", type="integer")
     */
    private $totalViews = 0;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Property247\MainBundle\Entity\Image", mappedBy="property")
     */
    private $images;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="reference_by", type="string", nullable=true)
     */
    private $referenceBy;

    /**
     * @var string
     * @ORM\Column(name="reference_contact_number", type="string", nullable=true)
     */
    private $referenceContactNumber;

    /**
     * @var string
     * @ORM\Column(name="owner_or_contact_person_name", type="string", nullable=true)
     */
    private $ownerOrContactPersonName;

    /**
     * @var string
     * @ORM\Column(name="owner_contact_number", type="string", nullable=true)
     */
    private $ownerContactNumber;

    /**
     * @var string
     * @ORM\Column(name="community_type", type="string", nullable=true)
     */
    private $communityType;

    /**
     * @var string
     * @ORM\Column(name="reason_for_sale", type="text", nullable=true)
     */
    private $reasonForSale;

    /**
     * @var File
     * @Vich\UploadableField(mapping="property_agreement_form", fileNameProperty="agreementFormImageFilename")
     */
    private $agreementFormImageFile;

    /**
     * @var string
     * @ORM\Column(name="agreement_form_image_filename", type="string", nullable=true)
     */
    private $agreementFormImageFilename;

    /**
     * @var bool
     * @ORM\Column(name="price_negotiable", type="boolean")
     */
    private $priceNegotiable = 0;

    /**
     * @var string
     * @ORM\Column(name="remarks", type="text", nullable=true)
     */
    private $remarks;

    use CreatedUpdatedOnTrait;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->floorPlannings = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $propertyType
     */
    public function setPropertyType($propertyType)
    {
        $this->propertyType = $propertyType;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSuitableFor()
    {
        return $this->suitableFor;
    }

    /**
     * @param string $suitableFor
     */
    public function setSuitableFor($suitableFor)
    {
        $this->suitableFor = $suitableFor;
    }

    /**
     * @return string
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param string $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getRoadAccess()
    {
        return $this->roadAccess;
    }

    /**
     * @param string $roadAccess
     */
    public function setRoadAccess($roadAccess)
    {
        $this->roadAccess = $roadAccess;
    }

    /**
     * @return string
     */
    public function getRoadAccessUnit()
    {
        return $this->roadAccessUnit;
    }

    /**
     * @param string $roadAccessUnit
     */
    public function setRoadAccessUnit($roadAccessUnit)
    {
        $this->roadAccessUnit = $roadAccessUnit;
    }

    /**
     * @return string
     */
    public function getBuiltDate()
    {
        return $this->builtDate;
    }

    /**
     * @param string $builtDate
     */
    public function setBuiltDate( $builtDate)
    {
        $this->builtDate = $builtDate;
    }

    /**
     * @return string
     */
    public function getTotalArea()
    {
        return $this->totalArea;
    }

    /**
     * @param string $totalArea
     */
    public function setTotalArea( $totalArea)
    {
        $this->totalArea = $totalArea;
    }

    /**
     * @return string
     */
    public function getAreaUnit()
    {
        return $this->areaUnit;
    }

    /**
     * @param string $areaUnit
     */
    public function setAreaUnit( $areaUnit)
    {
        $this->areaUnit = $areaUnit;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation( $location)
    {
        $this->location = $location;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity(City $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getHouseStructure()
    {
        return $this->houseStructure;
    }

    /**
     * @param string $houseStructure
     */
    public function setHouseStructure(string $houseStructure)
    {
        $this->houseStructure = $houseStructure;
    }

    /**
     * @return string
     */
    public function getHouseStructureDescription()
    {
        return $this->houseStructureDescription;
    }

    /**
     * @param string $houseStructureDescription
     */
    public function setHouseStructureDescription(string $houseStructureDescription)
    {
        $this->houseStructureDescription = $houseStructureDescription;
    }

    /**
     * @return string
     */
    public function getHouseBuildArea()
    {
        return $this->houseBuildArea;
    }

    /**
     * @param string $houseBuildArea
     */
    public function setHouseBuildArea( $houseBuildArea)
    {
        $this->houseBuildArea = $houseBuildArea;
    }

    /**
     * @return string
     */
    public function getHouseBuildAreaUnit()
    {
        return $this->houseBuildAreaUnit;
    }

    /**
     * @param string $houseBuildAreaUnit
     */
    public function setHouseBuildAreaUnit( $houseBuildAreaUnit)
    {
        $this->houseBuildAreaUnit = $houseBuildAreaUnit;
    }

    /**
     * @return string
     */
    public function getHouseFacedOn()
    {
        return $this->houseFacedOn;
    }

    /**
     * @param string $houseFacedOn
     */
    public function setHouseFacedOn( $houseFacedOn)
    {
        $this->houseFacedOn = $houseFacedOn;
    }

    /**
     * @return string
     */
    public function getFrontFace()
    {
        return $this->frontFace;
    }

    /**
     * @param string $frontFace
     */
    public function setFrontFace( $frontFace)
    {
        $this->frontFace = $frontFace;
    }

    /**
     * @return string
     */
    public function getFrontFaceUnit()
    {
        return $this->frontFaceUnit;
    }

    /**
     * @param string $frontFaceUnit
     */
    public function setFrontFaceUnit($frontFaceUnit)
    {
        $this->frontFaceUnit = $frontFaceUnit;
    }


    /**
     * @return string
     */
    public function getBackFace()
    {
        return $this->backFace;
    }

    /**
     * @param string $backFace
     */
    public function setBackFace( $backFace)
    {
        $this->backFace = $backFace;
    }

    /**
     * @return string
     */
    public function getBackFaceUnit()
    {
        return $this->backFaceUnit;
    }

    /**
     * @param string $backFaceUnit
     */
    public function setBackFaceUnit($backFaceUnit)
    {
        $this->backFaceUnit = $backFaceUnit;
    }

    /**
     * @return string
     */
    public function getRoadType()
    {
        return $this->roadType;
    }

    /**
     * @param string $roadType
     */
    public function setRoadType( $roadType)
    {
        $this->roadType = $roadType;
    }

    /**
     * @return string
     */
    public function getPrivateAmenities()
    {
        return $this->privateAmenities;
    }

    /**
     * @param string $privateAmenities
     */
    public function setPrivateAmenities($privateAmenities)
    {
        $this->privateAmenities = $privateAmenities;
    }

    /**
     * @return string
     */
    public function getDistanceFromMainRoad()
    {
        return $this->distanceFromMainRoad;
    }

    /**
     * @param string $distanceFromMainRoad
     */
    public function setDistanceFromMainRoad( $distanceFromMainRoad)
    {
        $this->distanceFromMainRoad = $distanceFromMainRoad;
    }

    /**
     * @return string
     */
    public function getDistanceMeasureType()
    {
        return $this->distanceMeasureType;
    }

    /**
     * @param string $distanceMeasureType
     */
    public function setDistanceMeasureType( $distanceMeasureType)
    {
        $this->distanceMeasureType = $distanceMeasureType;
    }

    /**
     * @return ArrayCollection
     */
    public function getFloorPlannings()
    {
        return $this->floorPlannings;
    }

    /**
     * @param $floorPlannings
     */
    public function setFloorPlannings($floorPlannings)
    {
        $this->floorPlannings = $floorPlannings;
    }

    /**
     * @param FloorPlan $floorPlanning
     */
    public function addFloorPlanning(FloorPlan $floorPlanning){
        $this->floorPlannings->add($floorPlanning);
    }

    /**
     * @param FloorPlan $floorPlan
     */
    public function removeFloorPlanning(FloorPlan $floorPlan){
        if($this->floorPlannings->contains($floorPlan)){
            $this->floorPlannings->removeElement($floorPlan);
        }
    }

    /**
     * @return bool
     */
    public function isSocailMediaPromotion(): bool
    {
        return $this->socailMediaPromotion;
    }

    /**
     * @param bool $socailMediaPromotion
     */
    public function setSocailMediaPromotion(bool $socailMediaPromotion)
    {
        $this->socailMediaPromotion = $socailMediaPromotion;
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    /**
     * @param PropertyDocument $propertyDocument
     */
    public function addDocument(PropertyDocument $propertyDocument){
        $propertyDocument->setProperty($this);
        $this->documents->add($propertyDocument);
    }

    /**
     * @param PropertyDocument $propertyDocument
     */
    public function removeDocument(PropertyDocument $propertyDocument){
        if($this->documents->contains($propertyDocument)){
            $this->documents->removeElement($propertyDocument);
        }
    }

    /**
     * @return bool
     */
    public function isFurnished()
    {
        return $this->furnished;
    }

    /**
     * @param bool $furnished
     */
    public function setFurnished( $furnished)
    {
        $this->furnished = $furnished;
    }

    /**
     * @return string
     */
    public function getPublicAmenities()
    {
        return $this->publicAmenities;
    }

    /**
     * @param string $publicAmenities
     */
    public function setPublicAmenities( $publicAmenities)
    {
        $this->publicAmenities = $publicAmenities;
    }

    /**
     * @return int
     */
    public function getTotalViews()
    {
        return $this->totalViews;
    }

    /**
     * @param int $totalViews
     */
    public function setTotalViews(int $totalViews)
    {
        $this->totalViews = $totalViews;
    }

    /**
     * @return User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param User $agent
     */
    public function setAgent(User $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return string
     */
    public function getTotalAreaInSqFt()
    {
        return $this->totalAreaInSqFt;
    }

    /**
     * @param string $totalAreaInSqFt
     */
    public function setTotalAreaInSqFt( $totalAreaInSqFt)
    {
        $this->totalAreaInSqFt = $totalAreaInSqFt;
    }

    /**
     * @return string
     */
    public function getHouseBuildAreaInSqFt()
    {
        return $this->houseBuildAreaInSqFt;
    }

    /**
     * @param string $houseBuildAreaInSqFt
     */
    public function setHouseBuildAreaInSqFt( $houseBuildAreaInSqFt)
    {
        $this->houseBuildAreaInSqFt = $houseBuildAreaInSqFt;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude( $latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude( $longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getTopWaterTank()
    {
        return $this->topWaterTank;
    }

    /**
     * @param int $topWaterTank
     */
    public function setTopWaterTank(int $topWaterTank)
    {
        $this->topWaterTank = $topWaterTank;
    }

    /**
     * @return int
     */
    public function getReserveWaterTank()
    {
        return $this->reserveWaterTank;
    }

    /**
     * @param int $reserveWaterTank
     */
    public function setReserveWaterTank(int $reserveWaterTank)
    {
        $this->reserveWaterTank = $reserveWaterTank;
    }

    /**
     * @return string
     */
    public function getStoreyed()
    {
        return $this->storeyed;
    }

    /**
     * @param string $storeyed
     */
    public function setStoreyed($storeyed)
    {
        $this->storeyed = $storeyed;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     */
    public function setApproved(bool $approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @param Image $image
     */
    public function addImage(Image $image){
        $this->images->add($image);
    }

    /**
     * @param Image $image
     */
    public function removeImage(Image $image){
        if($this->images->contains($image)){
            $this->images->removeElement($image);
        }
    }

    /**
     * @return string
     */
    public function getKittaNumber()
    {
        return $this->kittaNumber;
    }

    /**
     * @param string $kittaNumber
     */
    public function setKittaNumber(string $kittaNumber)
    {
        $this->kittaNumber = $kittaNumber;
    }

    /**
     * @return string
     */
    public function getWardNumberOnMap()
    {
        return $this->wardNumberOnMap;
    }

    /**
     * @param string $wardNumberOnMap
     */
    public function setWardNumberOnMap(string $wardNumberOnMap)
    {
        $this->wardNumberOnMap = $wardNumberOnMap;
    }

    /**
     * @return string
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /**
     * @param string $streetName
     */
    public function setStreetName(string $streetName)
    {
        $this->streetName = $streetName;
    }

    /**
     * @return string
     */
    public function getToleWardNumber()
    {
        return $this->toleWardNumber;
    }

    /**
     * @param string $toleWardNumber
     */
    public function setToleWardNumber(string $toleWardNumber)
    {
        $this->toleWardNumber = $toleWardNumber;
    }

    /**
     * @return bool
     */
    public function isPriceNegotiable(): bool
    {
        return $this->priceNegotiable;
    }

    /**
     * @param bool $priceNegotiable
     */
    public function setPriceNegotiable(bool $priceNegotiable)
    {
        $this->priceNegotiable = $priceNegotiable;
    }

    /**
     * @return string
     */
    public function getTotalFloorCoverage()
    {
        return $this->totalFloorCoverage;
    }

    /**
     * @param string $totalFloorCoverage
     */
    public function setTotalFloorCoverage(string $totalFloorCoverage)
    {
        $this->totalFloorCoverage = $totalFloorCoverage;
    }

    /**
     * @return string
     */
    public function getTotalFloorCoverageAreaUnit()
    {
        return $this->totalFloorCoverageAreaUnit;
    }

    /**
     * @param string $totalFloorCoverageAreaUnit
     */
    public function setTotalFloorCoverageAreaUnit(string $totalFloorCoverageAreaUnit)
    {
        $this->totalFloorCoverageAreaUnit = $totalFloorCoverageAreaUnit;
    }

    /**
     * @return string
     */
    public function getFloorCoverageAreaUnit()
    {
        return $this->floorCoverageAreaUnit;
    }

    /**
     * @param string $floorCoverageAreaUnit
     */
    public function setFloorCoverageAreaUnit(string $floorCoverageAreaUnit)
    {
        $this->floorCoverageAreaUnit = $floorCoverageAreaUnit;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getReferenceBy()
    {
        return $this->referenceBy;
    }

    /**
     * @param string $referenceBy
     */
    public function setReferenceBy($referenceBy)
    {
        $this->referenceBy = $referenceBy;
    }

    /**
     * @return string
     */
    public function getReferenceContactNumber()
    {
        return $this->referenceContactNumber;
    }

    /**
     * @param string $referenceContactNumber
     */
    public function setReferenceContactNumber($referenceContactNumber)
    {
        $this->referenceContactNumber = $referenceContactNumber;
    }

    /**
     * @return string
     */
    public function getOwnerOrContactPersonName()
    {
        return $this->ownerOrContactPersonName;
    }

    /**
     * @param string $ownerOrContactPersonName
     */
    public function setOwnerOrContactPersonName($ownerOrContactPersonName)
    {
        $this->ownerOrContactPersonName = $ownerOrContactPersonName;
    }

    /**
     * @return string
     */
    public function getOwnerContactNumber()
    {
        return $this->ownerContactNumber;
    }

    /**
     * @param string $ownerContactNumber
     */
    public function setOwnerContactNumber($ownerContactNumber)
    {
        $this->ownerContactNumber = $ownerContactNumber;
    }

    /**
     * @return string
     */
    public function getCommunityType()
    {
        return $this->communityType;
    }

    /**
     * @param string $communityType
     */
    public function setCommunityType($communityType)
    {
        $this->communityType = $communityType;
    }

    /**
     * @return string
     */
    public function getReasonForSale()
    {
        return $this->reasonForSale;
    }

    /**
     * @param string $reasonForSale
     */
    public function setReasonForSale($reasonForSale)
    {
        $this->reasonForSale = $reasonForSale;
    }

    /**
     * @return File
     */
    public function getAgreementFormImageFile()
    {
        return $this->agreementFormImageFile;
    }

    /**
     * @param File $agreementFormImageFile
     */
    public function setAgreementFormImageFile($agreementFormImageFile)
    {
        $this->agreementFormImageFile = $agreementFormImageFile;
    }

    /**
     * @return string
     */
    public function getAgreementFormImageFilename()
    {
        return $this->agreementFormImageFilename;
    }

    /**
     * @param string $agreementFormImageFilename
     */
    public function setAgreementFormImageFilename($agreementFormImageFilename)
    {
        $this->agreementFormImageFilename = $agreementFormImageFilename;
    }

    /**
     * @return string
     */
    public function getMainRoadName()
    {
        return $this->mainRoadName;
    }

    /**
     * @param string $mainRoadName
     */
    public function setMainRoadName($mainRoadName)
    {
        $this->mainRoadName = $mainRoadName;
    }

    /**
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param string $remarks
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }

    public function __toString()
    {
        return $this->name ? $this->name : '';
    }

}

