<?php

namespace Property247\PropertyBundle\Form;

use Doctrine\ORM\EntityRepository;
use Property247\PropertyBundle\Entity\Amenity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FloorPlanAmenityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('quantity')
            ->add('amenity', EntityType::class, [
                'class' => Amenity::class,
                'placeholder' => 'Floor Configuration',
                'query_builder' => function(EntityRepository $e){
                    return $e->createQueryBuilder('a')
                            ->where('a.type = :type')->setParameter('type', Amenity::AMENITY_TYPE_INTERNAL)
                        ;
                }
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\PropertyBundle\Entity\FloorPlanAmenity'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_propertybundle_floorplanamenity';
    }


}
