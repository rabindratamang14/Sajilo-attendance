<?php

namespace Property247\PropertyBundle\Form;

use Property247\PropertyBundle\Entity\Amenity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AmenityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('iconImage', VichImageType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'file-styled',
                    'data-browse-class' => 'btn btn-default btn-xs'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(Amenity::$typeArray)
            ])
            ->add('description');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\PropertyBundle\Entity\Amenity',
            'isUpdating' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_propertybundle_amenity';
    }


}
