<?php

namespace Property247\PropertyBundle\Form;

use Doctrine\ORM\EntityRepository;
use Property247\PropertyBundle\Constants\Property;
use Property247\PropertyBundle\Entity\Amenity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FloorPlanType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('floorName')
            ->add('totalArea')
            ->add('areaUnit', ChoiceType::class, [
                'placeholder' => 'Area Unit',
                'choices' => array_flip(Property::$floorAreaTypeArray)
            ])
//            ->add('configuration')
            ->add('image2DFile')
            ->add('floorPlanAmenities', CollectionType::class, [
                'entry_type' => FloorPlanAmenityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\PropertyBundle\Entity\FloorPlan'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_propertybundle_floorplan';
    }


}
