<?php

namespace Property247\PropertyBundle\Form;

use Property247\PropertyBundle\Entity\Amenity;
use Property247\PropertyBundle\Service\AmenityService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class AmenityPrototypeType
 * @package Property247\PropertyBundle\Form
 * @DI\Service("property.amenity.prototype.form")
 * @DI\Tag("form.type")
 */
class AmenityPrototypeType extends AbstractType
{

    /**
     * @var AmenityService
     * @DI\Inject("property.amenity.service")
     */
    var $amenityService;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', ChoiceType::class, [
            'placeholder' => 'Amenity Type',
            'label' => false,
            'choices' => $this->amenityService->getAmenityListArray(['type' => Amenity::AMENITY_TYPE_INTERNAL]),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\PropertyBundle\Entity\Amenity',
            'isUpdating' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_propertybundle_amenity';
    }


}
