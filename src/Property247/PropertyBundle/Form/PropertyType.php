<?php

namespace Property247\PropertyBundle\Form;

use Doctrine\ORM\EntityRepository;
use Property247\PropertyBundle\Entity\Amenity;
use Property247\UserBundle\Entity\User;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Property247\PropertyBundle\Constants\Property;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use YarshaStudio\LocalityBundle\Entity\City;

class PropertyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $propertyTypeArray = $options['propertyType'] == Property::PROPERTY_TYPE_HOUSE ? Property::$houseTypesArray : Property::$landTypeArray;

        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('type', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Select type',
                'choices' => array_flip($propertyTypeArray)
            ])
            ->add('suitableFor', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Select purpose',
                'choices' => array_flip(Property::$purposesArray)
            ])
            ->add('price', NumberType::class, [
                'required' => false
            ])
            ->add('latitude', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'latitude'
                ]
            ])
            ->add('longitude', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'longitude'
                ]
            ])
            ->add('streetName')
            ->add('toleWardNumber', TextType::class, [
                'required' => false,
            ])
            ->add('kittaNumber')
            ->add('wardNumberOnMap')
            ->add('location', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 5
                ]
            ])
            ->add('city', EntityType::class, [
                'class' => City::class,
                'required' => false,
                'query_builder' => function (EntityRepository $e) {
                    return $e->createQueryBuilder('c')
                        ->where('c.deleted = :deleted')
                        ->setParameter('deleted', false);
                },
                'placeholder' => 'Select city',
                'choice_attr' => function (City $city) {
                    return [
                        'data-city-type' => $city->getType(),
                        'data-district-id' => $city->getDistrict() ? $city->getDistrict()->getId() : 0,
                        'data-province-id' => $city->getProvince() ? $city->getProvince()->getId() : 0,
                        'class' => 'select-city-option'
                    ];
                }
            ])
            ->add('roadAccess', TextType::class, [
                'required' => false
            ])
            ->add('priceNegotiable', CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'price-negotiable'
                ]
            ])
            ->add('roadAccessUnit', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Unit',
                'choices' => array_flip(Property::$distanceMeasureTypes)
            ])
            ->add('roadType', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Road type',
                'choices' => array_flip(Property::$roadTypeArray)
            ])
            ->add('distanceFromMainRoad', TextType::class, [
                'required' => false
            ])
            ->add('distanceMeasureType', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Measure Type',
                'choices' => array_flip(Property::$distanceMeasureTypes)
            ])
            ->add('totalArea', TextType::class, [
                'required' => false,
                'label' => 'Total area',
                'attr' => [
                    'class' => 'total-area-mask',
                    'data-mask' => '00-00-00-00',
                    'placeholder' => '00-00-00-00'
                ]
            ])
            ->add('areaUnit', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip(Property::$areaFormats)
            ])
            ->add('frontFace', TextType::class, [
                'required' => false
            ])
            ->add('frontFaceUnit', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Measure Unit',
                'choices' => array_flip(Property::$distanceMeasureTypes)
            ])
            ->add('backFace', TextType::class, [
                'required' => false
            ])
            ->add('backFaceUnit', ChoiceType::class, [
                'required' => false,
                'placeholder' => 'Measure Unit',
                'choices' => array_flip(Property::$distanceMeasureTypes)
            ])
            ->add('remarks')
            ->add('mainRoadName')
        ;
//            ->add('socialMediaSetting', SocialMediaSettingType::class);

        if ($options['propertyType'] == Property::PROPERTY_TYPE_HOUSE) {
            $builder
                ->add('topWaterTank')
                ->add('reserveWaterTank')
                ->add('storeyed')
                ->add('houseStructure', ChoiceType::class, [
                    'required' => false,
                    'placeholder' => 'Select house structure',
                    'choices' => array_flip(Property::$houseStructureArray)
                ])
                ->add('houseStructureDescription', TextareaType::class, [
                    'required' => false
                ])
                ->add('houseBuildArea', TextType::class, [
                    'required' => false,
                    'label' => 'House Build Area (Sq. ft)'
                ])
                ->add('houseBuildAreaUnit', HiddenType::class, [
                    'required' => false,
                    'attr' => [
                        'value' => Property::FLOOR_AREA_TYPE_SQ_FT
                    ]
                ])
                ->add('totalFloorCoverage', TextType::class, [
                    'required' => false,
                    'label' => 'Total Floor Coverage (Sq.ft)'
                ])
                ->add('floorCoverageAreaUnit', HiddenType::class, [
                    'required' => false,
                    'attr' => [
                        'value' => Property::FLOOR_AREA_TYPE_SQ_FT
                    ]
                ])
                ->add('houseFacedOn', ChoiceType::class, [
                    'required' => false,
                    'placeholder' => 'House Faced On',
                    'choices' => array_flip(Property::$directionsArray)
                ])
                ->add('builtDate', TextType::class, [
                    'required' => false,
                    'attr' => [
                        'class' => 'datetimepicker'
                    ]
                ])
            ;
        }

        if ($options['isUpdating'] == true) {
            $builder
                ->add('socailMediaPromotion')
                ->add('email', EmailType::class,[
                    'required' => false
                ])
                ->add('referenceBy')
                ->add('referenceContactNumber')
                ->add('ownerOrContactPersonName')
                ->add('ownerContactNumber')
                ->add('communityType')
                ->add('reasonForSale')
                ->add('agreementFormImageFile', VichImageType::class,[
                    'required' => false
                ])
                ->add('privateAmenities', EntityType::class, [
                    'class' => Amenity::class,
                    'required' => false,
                    'placeholder' => 'Select Private Amenity',
                    'query_builder' => function (EntityRepository $e) {
                        return $e->createQueryBuilder('a')
                            ->where('a.type = :type')->setParameter('type', Amenity::AMENITY_TYPE_PRIVATE)
                            ->andWhere('a.deleted = :deleted')->setParameter('deleted', false);
                    },
                    'expanded' => true,
                    'multiple' => true
                ])
                ->add('publicAmenities', EntityType::class, [
                    'class' => Amenity::class,
                    'placeholder' => 'Select Public Amenity',
                    'required' => false,
                    'query_builder' => function (EntityRepository $e) {
                        return $e->createQueryBuilder('a')
                            ->where('a.type = :type')->setParameter('type', Amenity::AMENITY_TYPE_PUBLIC)
                            ->andWhere('a.deleted = :deleted')->setParameter('deleted', false);
                    },
                    'expanded' => true,
                    'multiple' => true
                ])
                ->add('documents', CollectionType::class, [
                    'entry_type' => PropertyDocumentType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'delete_empty' => true,
                    'label' => false,
                    'by_reference' => false,
                    'entry_options' => [
                        'label' => false
                    ]
                ])
            ;
        }

        if ($options['propertyType'] == Property::PROPERTY_TYPE_HOUSE and $options['isUpdating'] == true) {
            $builder
                ->add('floorPlannings', CollectionType::class, [
                    'entry_type' => FloorPlanType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'delete_empty' => true,
                    'label' => false,
                    'entry_options' => [
                        'label' => false
                    ]
                ]);
        }

        if (
            $options['user'] &&
            $options['user'] instanceof User &&
            in_array('ROLE_SUPER_ADMIN', $options['user']->getRoles())
        ) {
            $builder->add('agent', EntityType::class, [
                'required' => false,
                'placeholder' => 'Select agent',
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('u');
                    return $qb->where('u.enabled = :enabled')->setParameter('enabled', true)
                        ->andWhere(
                            $qb->expr()->orX(
                                $qb->expr()->like('u.roles', $qb->expr()->literal('%ROLE_AGENT%')),
                                $qb->expr()->like('u.roles', $qb->expr()->literal('%ROLE_AGENT_ADMIN%'))
                            )
                        );
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\PropertyBundle\Entity\Property',
            'propertyType' => Property::PROPERTY_TYPE_HOUSE,
            'user' => null,
            'isUpdating' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_propertybundle_property';
    }


}
