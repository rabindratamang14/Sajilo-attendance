<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/19/18
 * Time: 3:54 PM
 */

namespace Property247\PropertyBundle\Service;

use Knp\Component\Pager\Paginator;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Entity\Image;
use Doctrine\ORM\EntityManagerInterface;
use Property247\PropertyBundle\Entity\Property;
use Symfony\Component\HttpFoundation\RequestStack;
use Dmishh\SettingsBundle\Manager\SettingsManagerInterface;
use Property247\PropertyBundle\Constants\Property as Constants;

/**
 * Class PropertyService
 * @package Property247\PropertyBundle\Service
 * @DI\Service("property.service", public=true)
 */
class PropertyService
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var SettingsManagerInterface
     * @DI\Inject("settings_manager")
     */
    public $settingService;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $requestStack;

    /**
     * @var Paginator
     * @DI\Inject("knp_paginator")
     */
    public $paginator;

    public function getPropertyList($filters = []){
        $repo = $this->em->getRepository(Property::class)->getPropertyListQuery($filters);
        return $repo->getQuery()->getResult();
    }

    public function getPaginatedPropertyList($filters = []){
        $query = $this->em->getRepository(Property::class)->getPropertyListQuery($filters);
        $limit = array_key_exists('limit', $filters) ? $filters['limit'] : 10;
        return $this->paginator->paginate(
            $query,
            $this->requestStack->getCurrentRequest()->get('page', 1),
            $limit
        );
    }

    public function getPropertyById($id){
        return $this->em->getRepository(Property::class)->find($id);
    }

    public function convertAreaToSquareFeet(int $area, string $type){
        $calculatedArea = $area;
        switch ($type){
            case Constants::AREA_TYPE_BIGHA:
                $calculatedArea = $area * 72900;
                break;
            case Constants::AREA_TYPE_ROPANI:
                $calculatedArea = $area * 5476;
                break;
            case Constants::AREA_TYPE_KATTHA:
                $calculatedArea = $area * 3645;
                break;
            case Constants::AREA_TYPE_AANA:
                $calculatedArea = $area * 342.25;
                break;
            case Constants::AREA_TYPE_DHUR:
                $calculatedArea = $area * 182.25;
                break;
            case Constants::AREA_TYPE_PAISA:
                $calculatedArea = $area * 85.56;
                break;
            case Constants::AREA_TYPE_DAAM:
                $calculatedArea = $area * 21.39;
                break;
        }
        return $calculatedArea;
    }

    public function countProperty($type, $filters = []){
        return $this->em->getRepository(Property::class)->countProperty($type, $filters);
    }

    public function savePropertyId(Property $property){

        $propertyIdLabel = Constants::PROPERTY_TYPE_LAND === $property->getType() ? "land_id_prefix" : "housing_id_prefix";
        $propertyIdPrefix = $this->settingService->get($propertyIdLabel);
        $defaultIdPrefix = Constants::PROPERTY_TYPE_LAND === $property->getType() ? "PPL" : "PPH";
        $propertyIdPrefix = $propertyIdPrefix ? $propertyIdPrefix : $defaultIdPrefix;
        $property->setPropertyId("{$propertyIdPrefix} {$property->getId()}");
        $this->em->persist($property);
        try{
            $this->em->flush();
            return true;
        }   catch (\Throwable $e){
            return false;
        }

    }

    public function addImages($imageIds, Property $property){
        $imageIds = $imageIds ? $imageIds : [];
        foreach ($imageIds as $i) {
            $image = $this->em->getRepository(Image::class)->findOneBy([
                'deleted' => false,
                'id' => $i
            ]);
            if($image instanceof Image){
                $image->setProperty($property);
            }
            $this->em->persist($image);
        }
        $this->em->flush();
    }

}