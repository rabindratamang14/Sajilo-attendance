<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/19/18
 * Time: 3:54 PM
 */

namespace Property247\PropertyBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\PropertyBundle\Entity\Amenity;
use Property247\MainBundle\Service\PaginationService;

/**
 * Class PropertyService
 * @package Property247\PropertyBundle\Service
 * @DI\Service("property.amenity.service", public=true)
 * @DI\Tag("form.type")
 */
class AmenityService extends PaginationService
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    public function getPaginatedAmenities($filters = [], $limit = 20){
        $qb = $this->em->getRepository(Amenity::class)->getAminetyListQuery($filters);
        return $this->paginate($qb, $limit);
    }

    public function getAmenityListArray($filters = []){
        $qb = $this->em->getRepository(Amenity::class)->getAminetyListQuery($filters);
        $amenities = $qb->getQuery()->getResult();
        $data = [];
        /**
         * @var Amenity $a
         */
        foreach ($amenities as $a){
            $data[] = [$a->getName() => $a->getName()];
        }
        return $data;
    }

}