<?php

namespace Property247\SettingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialMediaSettingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shareOnFacebook')
            ->add('shareOnTwitter')
            ->add('sendEmailToFriend')
            ->add('shareOnGooglePlus')
            ->add('shareOnFacebookMessenger')
            ->add('shareOnViber')
            ->add('shareOnWhatsapp')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\SettingBundle\Entity\SocialMediaSetting'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_settingbundle_socialmediasetting';
    }


}
