<?php

namespace Property247\SettingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\UserBundle\Entity\User;

/**
 * Options
 *
 * @ORM\Table(name="options")
 * @ORM\Entity(repositoryClass="Property247\SettingBundle\Repository\OptionsRepository")
 */
class Options
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="options_name", type="string", length=255, nullable=true)
     */
    private $optionsName;

    /**
     * @var string
     *
     * @ORM\Column(name="options_value", type="text")
     */
    private $optionsValue;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Property247\UserBundle\Entity\User")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOptionsName()
    {
        return $this->optionsName;
    }

    /**
     * @param string $optionsName
     */
    public function setOptionsName(string $optionsName)
    {
        $this->optionsName = $optionsName;
    }

    /**
     * @return string
     */
    public function getOptionsValue()
    {
        return $this->optionsValue;
    }

    /**
     * @param string $optionsValue
     */
    public function setOptionsValue(string $optionsValue)
    {
        $this->optionsValue = $optionsValue;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

}

