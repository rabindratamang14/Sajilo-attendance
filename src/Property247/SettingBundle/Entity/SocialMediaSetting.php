<?php

namespace Property247\SettingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\PropertyBundle\Entity\Property;
use Property247\UserBundle\Entity\User;

/**
 * SocialMediaSetting
 *
 * @ORM\Table(name="social_media_setting")
 * @ORM\Entity(repositoryClass="Property247\SettingBundle\Repository\SocialMediaSettingRepository")
 */
class SocialMediaSetting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="share_on_facebook", type="boolean")
     */
    private $shareOnFacebook = false;

    /**
     * @var boolean
     * @ORM\Column(name="share_on_twitter", type="boolean")
     */
    private $shareOnTwitter = false;

    /**
     * @var boolean
     * @ORM\Column(name="send_email_to_friend", type="boolean")
     */
    private $sendEmailToFriend = false;

    /**
     * @var boolean
     * @ORM\Column(name="share_on_google_plus", type="boolean")
     */
    private $shareOnGooglePlus = false;

    /**
     * @var bool
     * @ORM\Column(name="share_on_facebook_messenger", type="boolean")
     */
    private $shareOnFacebookMessenger = false;

    /**
     * @var bool
     * @ORM\Column(name="share_on_viber", type="boolean")
     */
    private $shareOnViber = false;

    /**
     * @var bool
     * @ORM\Column(name="share_on_whatsapp", type="boolean")
     */
    private $shareOnWhatsapp = false;

//    /**
//     * @var Property
//     * @ORM\OneToOne(targetEntity="Property247\PropertyBundle\Entity\Property", inversedBy="socialMediaSetting")
//     */
//    private $property;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isShareOnFacebook(): bool
    {
        return $this->shareOnFacebook;
    }

    /**
     * @param bool $shareOnFacebook
     */
    public function setShareOnFacebook(bool $shareOnFacebook)
    {
        $this->shareOnFacebook = $shareOnFacebook;
    }

    /**
     * @return bool
     */
    public function isShareOnTwitter(): bool
    {
        return $this->shareOnTwitter;
    }

    /**
     * @param bool $shareOnTwitter
     */
    public function setShareOnTwitter(bool $shareOnTwitter)
    {
        $this->shareOnTwitter = $shareOnTwitter;
    }

    /**
     * @return bool
     */
    public function isSendEmailToFriend(): bool
    {
        return $this->sendEmailToFriend;
    }

    /**
     * @param bool $sendEmailToFriend
     */
    public function setSendEmailToFriend(bool $sendEmailToFriend)
    {
        $this->sendEmailToFriend = $sendEmailToFriend;
    }

    /**
     * @return bool
     */
    public function isShareOnGooglePlus(): bool
    {
        return $this->shareOnGooglePlus;
    }

    /**
     * @param bool $shareOnGooglePlus
     */
    public function setShareOnGooglePlus(bool $shareOnGooglePlus)
    {
        $this->shareOnGooglePlus = $shareOnGooglePlus;
    }

    /**
     * @return bool
     */
    public function isShareOnFacebookMessenger(): bool
    {
        return $this->shareOnFacebookMessenger;
    }

    /**
     * @param bool $shareOnFacebookMessenger
     */
    public function setShareOnFacebookMessenger(bool $shareOnFacebookMessenger)
    {
        $this->shareOnFacebookMessenger = $shareOnFacebookMessenger;
    }

    /**
     * @return bool
     */
    public function isShareOnViber(): bool
    {
        return $this->shareOnViber;
    }

    /**
     * @param bool $shareOnViber
     */
    public function setShareOnViber(bool $shareOnViber)
    {
        $this->shareOnViber = $shareOnViber;
    }

    /**
     * @return bool
     */
    public function isShareOnWhatsapp(): bool
    {
        return $this->shareOnWhatsapp;
    }

    /**
     * @param bool $shareOnWhatsapp
     */
    public function setShareOnWhatsapp(bool $shareOnWhatsapp)
    {
        $this->shareOnWhatsapp = $shareOnWhatsapp;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty(Property $property)
    {
        $this->property = $property;
    }

}

