<?php

namespace Propery247\MainBundle\Listener;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpFoundation\RequestStack;
use Property247\MainBundle\Annotations\Permissions;
use Property247\MainBundle\Service\PermissionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class ControllerListener
 * @package  Propery247\MainBundle\Listener
 *
 * @DI\Service("property.listener.controller", autowire=true)
 * @DI\Tag(name="kernel.event_listener", attributes={"event"="kernel.controller", "method"="onFilterController"})
 */
class ControllerListener
{

    /**
     * @var Reader
     * @DI\Inject("annotations.reader")
     */
    public $reader;

    /**
     * @var PermissionService
     * @DI\Inject("property.service.permission")
     */
    public $permissionService;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $request;

    /**
     * @param FilterControllerEvent $event
     */
    public function onFilterController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        list($object, $method) = $controller;

        if($object instanceof Controller)
        {
            // the controller could be a proxy, e.g. when using the JMSSecurityExtraBundle or JMSDiExtraBundle
            $className = ClassUtils::getClass($object);

            $reflectionClass = new \ReflectionClass($className);
            $reflectionMethod = $reflectionClass->getMethod($method);

            $allAnnotations = $this->reader->getMethodAnnotations($reflectionMethod);

            $permissionAnnotations = array_filter($allAnnotations, function($annotation) {
                return $annotation instanceof Permissions;
            });

            $requiredPermissions = [];

            foreach($permissionAnnotations as $permissionAnnotation)
            {
                $routeFromAnnotation = $permissionAnnotation->getRoute();

                if( !$routeFromAnnotation or $routeFromAnnotation == $this->request->getCurrentRequest()->get('_route'))
                {
                    $requiredPermissions[] = $permissionAnnotation->value;
                }
            }

            if( count($requiredPermissions) )
            {
                $hasPermission = $this->permissionService->hasPermission($requiredPermissions);

                if( ! $hasPermission )
                {
                    throw new AccessDeniedException();
                }
            }
        }

    }

}