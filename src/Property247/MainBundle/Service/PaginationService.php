<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/20/18
 * Time: 11:10 PM
 */

namespace Property247\MainBundle\Service;

use JMS\DIExtraBundle\Annotation as DI;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaginationService
 * @package Property247\MainBundle\Service
 * @DI\Service("main.pagination.service", public=true)
 */
class PaginationService
{

    /**
     * @var Paginator
     * @DI\Inject("knp_paginator")
     */
    public $paginator;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $request;

    public function paginate($query, $limit = 10){
        return $this->paginator->paginate(
            $query,
            $this->request->getCurrentRequest()->get('page',1),
            $limit
        );
    }

}