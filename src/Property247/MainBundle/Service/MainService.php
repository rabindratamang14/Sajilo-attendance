<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/22/18
 * Time: 3:14 PM
 */

namespace Property247\MainBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class MainService
 * @package Property247\MainBundle\Service
 * @DI\Service("main.service", public=true)
 */
class MainService
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var PaginatorInterface
     * @DI\Inject("knp_paginator")
     */
    public $paginator;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $requestStack;

    public function paginate($query, $limit = 10, $options = []){
        return $this->paginator->paginate(
            $query,
            $this->requestStack->getCurrentRequest()->get('page', 1),
            $limit,
            $options
        );
    }

}