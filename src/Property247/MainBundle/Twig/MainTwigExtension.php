<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/21/18
 * Time: 4:22 PM
 */

namespace Property247\MainBundle\Twig;

use Twig\Extension\AbstractExtension;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class MainTwigExtension
 * @package Property247\MainBundle\Twig
 * @DI\Service("main.twig.extension", public=true)
 * @DI\Tag("twig.extension")
 */
class MainTwigExtension extends AbstractExtension
{

    public function getFunctions(){
        return [
            new \Twig_SimpleFunction('no_contents',[$this,'noContents'],['is_safe' => ['html']])
        ];
    }

    public function noContents($message = "No contents available."){
        $out = '<div class="col-lg-12"><br/><br/>';
        $out .= '<div class="text-danger"><i class="fa fa-exclamation-triangle"></i> ' . $message . '</div>';
        $out .= '<br/><br/></div>';
        return $out;
    }

}