<?php

namespace Property247\AttendanceBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use Property247\AttendanceBundle\Entity\Subject;
use Property247\AttendanceBundle\Form\SubjectType;
use Property247\AttendanceBundle\Service\SubjectService;
use Property247\MainBundle\Annotations\Permissions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SubjectController
 * @package Property247\AttendanceBundle\Controller
 * @Route("/admin")
 */
class SubjectController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @var SubjectService
     * @DI\Inject("property.subject.service")
     */
    private $subjectService;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    private $apy;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/subject", name="admin_subject_list")
     * @Breadcrumb("Subject", routeName="admin_subject_list")
     * @Breadcrumb("List")
     */
    public function listAction(Request $request){
        $filters = $request->query->all();
        $data['subjects'] = $this->subjectService->getPaginatedSubjectList($filters);
        return $this->render('@Property247Attendance/Subject/list.html.twig', $data);
    }

    /**
    * @param Request $request
    * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
    * @Route("/subject/add", name="admin_subject_add")
    * @Route("/subject/{id}/update", name="admin_subject_update")
    * @Breadcrumb("Subject", routeName="admin_subject_list")
    */
    public function addAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        if($id){
            $subject= $this->em->getRepository(Subject::class)->find($id);
            if(! $subject instanceof Subject){
                return $this->redirectToRoute('admin_subject_add');
            }
            $this->apy->add($subject->getName());
            $this->apy->add('Update');
            $message = "subject updated.";
            $data['isUpdating'] = true;
        }   else    {
            $this->apy->add('Add');
            $subject = new Subject();
            $message = "Subject added.";
        }

        $form = $this->createForm(SubjectType::class, $subject, $data);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /**
             * @var $formData Subject
             */
            $formData = $form->getData();

            $this->em->persist($formData);

            try{
                $this->em->flush();
                $this->addFlash('success', $message);
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirectToRoute('admin_subject_list');
        }
        $data['form'] = $form->createView();
        return $this->render('@Property247Attendance/Subject/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/subject/{id}/delete", name="admin_delete_subject")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        if($id){
            $subject = $this->em->getRepository(Subject::class)->find($id);
            if($subject instanceof Subject){
                $subject->setDeleted(true);
                $this->em->persist($subject);
                try{
                    $this->em->flush();
                    $this->addFlash('success', 'Subject deleted.');
                }   catch (\Throwable $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirectToRoute('admin_student_list');
    }
}
