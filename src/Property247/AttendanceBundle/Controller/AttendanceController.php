<?php

namespace Property247\AttendanceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class HomeController
 * @package Property247\AdminBundle\Controller
 * @Route("/student")
 */
class AttendanceController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/ ", name="student_dashboard")
     */
    public function indexAction(Request $request)
    {
        return $this->render('@Property247Attendance/Default/index.html.twig');
    }
}
