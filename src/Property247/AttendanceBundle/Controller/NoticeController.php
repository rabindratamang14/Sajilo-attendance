<?php

namespace Property247\AttendanceBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use Property247\AttendanceBundle\Entity\Notice;
use Property247\AttendanceBundle\Form\NoticeType;
use Property247\AttendanceBundle\Service\NoticeService;
use Property247\AttendanceBundle\Service\SubjectService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SubjectController
 * @package Property247\AttendanceBundle\Controller
 * @Route("/admin")
 */
class NoticeController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @var NoticeService
     * @DI\Inject("property.notice.service")
     */
    private $noticeService;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    private $apy;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/Notice", name="admin_notice_list")
     * @Breadcrumb("Notice", routeName="admin_notice_list")
     * @Breadcrumb("List")
     */
    public function listAction(Request $request){
        $filters = $request->query->all();
        $data['notices'] = $this->noticeService->getPaginatedNoticeList($filters);
        return $this->render('@Property247Attendance/Notice/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/notice/add", name="admin_notice_add")
     * @Route("/notice/{id}/update", name="admin_notice_update")
     * @Breadcrumb("notice", routeName="admin_notice_list")
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        if($id){
            $notice = $this->em->getRepository(Notice::class)->find($id);
            if(! $notice instanceof Notice){
                return $this->redirectToRoute('admin_notice_add');
            }
            $this->apy->add($notice->getTitle());
            $this->apy->add('Update');
            $message = "Notice updated.";
            $data['isUpdating'] = true;
        }   else    {
            $this->apy->add('Add');
            $notice = new Notice();
            $message = "Notice added.";
        }

        $form = $this->createForm(NoticeType::class, $notice, $data);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /**
             * @var $formData Notice
             */
            $formData = $form->getData();

            $this->em->persist($formData);

            try{
                $this->em->flush();
                $this->addFlash('success', $message);
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirectToRoute('admin_notice_list');
        }
        $data['form'] = $form->createView();
        return $this->render('@Property247Attendance/Notice/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/notice/{id}/delete", name="admin_notice_delete")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        if($id){
            $notice = $this->em->getRepository(Notice::class)->find($id);
            if($notice instanceof Notice){
                $notice->setDeleted(true);
                $this->em->persist($notice);
                try{
                    $this->em->flush();
                    $this->addFlash('success', 'Notice deleted.');
                }   catch (\Throwable $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirectToRoute('admin_notice_list');
    }
}
