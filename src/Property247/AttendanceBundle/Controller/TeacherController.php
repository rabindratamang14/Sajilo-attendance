<?php

namespace Property247\AttendanceBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use Property247\AttendanceBundle\Entity\Attendance;
use Property247\AttendanceBundle\Entity\Subject;
use Property247\AttendanceBundle\Service\TeacherService;
use Property247\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;


/**
 * Class HomeController
 * @package Property247\AdminBundle\Controller
 * @Route("/teacher")
 */
class TeacherController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;


    /**
     * @var TeacherService
     * @DI\Inject("property.teacher.service")
     */
    private $teacherService;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    private $apy;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/ ", name="teacher_dashboard")
     */
    public function indexAction()
    {
        return $this->render('@Property247Attendance/teacher/index.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/subject ", name="teacher_subject_list")
     * @Breadcrumb("Subject", routeName="teacher_subject_list")
     * @Breadcrumb("List")
     */
    public function subjectAction()
    {

        $user=$this->getUser();
        $filter['userId']='';
        $data['subjects'] = $this->teacherService->getPaginatedSubjectList($filter,$user);
        return $this->render('@Property247Attendance/teacher/subject.html.twig',$data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/subject/{id}", name="teacher_subject_attendance")
     * @Breadcrumb("Subject")
     * @Breadcrumb("Attendance")
     */
    public function subjectAttendanceAction(Request $request)
    {
        $id = $request->get('id');
        $subject= $this->em->getRepository(Subject::class)->find($id);
        $semester=$subject->getSemester();
        $students = $this->em->getRepository(User::class)->findStudentBySemester($semester);
        $count_std=count($students);
        $created_ats=$this->em->getRepository(Attendance::class)->findCreatedAt($subject,$students[0]);
        $num_recent_dates = sizeof($created_ats);
        $total_created_ats=$this->em->getRepository(Attendance::class)->findTotalCreatedAt($subject,$students[0]);
        $total_attendance_days = sizeof($total_created_ats);

        for ($i=0; $i<$count_std; $i++){
            $attendances[$i] = $this->em->getRepository(Attendance::class)->findAttendance($subject,$students[$i],$num_recent_dates);
            $total_presence[$i] = $this->em->getRepository(Attendance::class)->totalPresence($subject,$students[$i]);
            $total_present[$i] = sizeof($total_presence[$i]);
        }

        // displaying only month and day
        for($i=0; $i<$num_recent_dates; $i++) {
            $date[$i] = preg_replace("/[0-9]{2}:[0-9]{2}:[0-9]{2}/","", $created_ats[$i]['createdOn']->format('Y-m-d H:i:s'));
            $date[$i] = preg_replace("/[0-9]{4}-/","", $date[$i]);
        }


        $todayDate=new \DateTime('now');
        $today=$todayDate->format("Y-m-d");
//
//        return view('show-attendance',compact('subject','students','date','attendances','count_std','num_recent_dates','today','total_present','total_attendance_days'));

        return $this->render('@Property247Attendance/teacher/attendance.html.twig',compact('subject','students','date','attendances','count_std','num_recent_dates','today','total_present','total_attendance_days'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}/take-attendance", name="teacher_take_attendance")
     */
    public function takeAttendanceAction(Request $request)
    {
        $id=$request->get('id');
        $subject= $this->em->getRepository(Subject::class)->find($id);
        $semester=$subject->getSemester();
        $students = $this->em->getRepository(User::class)->findStudentBySemester($semester);
        $count_std=count($students);
        $created_ats=$this->em->getRepository(Attendance::class)->findCreatedAt($subject,$students[0]);
        $num_recent_dates = sizeof($created_ats);
        for($i=0; $i<$num_recent_dates; $i++) {
            $date[$i] = preg_replace("/[0-9]{2}:[0-9]{2}:[0-9]{2}/","", $created_ats[$i]['createdOn']->format('Y-m-d H:i:s'));
        }
        if (in_array($request->get('date').'', $date)){
            $this->addFlash('error', "Today's Attendance is already taken !!!");
            return $this->redirectToRoute('teacher_subject_attendance',array('id' => $id));
        }

        for($i=0; $i<$count_std; $i++){
            $attendance = new Attendance();
            $attendance->setAttendance($request->get('attend'.$i));
            $attendance->setSubject($subject);
            $attendance->setStudent($students[$i]);
            $this->em->persist($attendance);
        }
        $this->em->flush();
        $this->addFlash('success', "Attendance completed");
        return $this->redirectToRoute('teacher_subject_attendance',array('id' => $id));

    }
}


