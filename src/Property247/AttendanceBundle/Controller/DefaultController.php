<?php

namespace Property247\AttendanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('Property247AttendanceBundle:Default:index.html.twig');
    }
}
