<?php
/**
 * Created by PhpStorm.
 * User: rabindra
 * Date: 9/13/18
 * Time: 8:52 PM
 */

namespace Property247\AttendanceBundle\Twig;


use Property247\AttendanceBundle\Service\SubjectService;
use Property247\UserBundle\Service\UserService;
use Twig\Extension\AbstractExtension;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AttendanceExtension
 * @package Property247\AttendanceBundle\Twig
 * @DI\Service("attendance.extension", public=true)
 * @DI\Tag("twig.extension")
 */
class AttendanceExtension  extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var SubjectService
     * @DI\Inject("property.subject.service")
     */
    public $subjectService;

    /**
     * @var UserService
     * @DI\Inject("property.user.service")
     */
    public $userService;

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('count_subject', [$this, 'countSubject']),
            new \Twig_SimpleFunction('count_student', [$this, 'countStudent']),
            new \Twig_SimpleFunction('count_teacher', [$this, 'countTeacher']),
        ];
    }

    public function countSubject(){
        return $this->subjectService->countSubjects();
    }

    public function countStudent(){
        return $this->userService->countStudent();
    }

    public function countTeacher(){
        return $this->userService->countTeacher();
    }
}

