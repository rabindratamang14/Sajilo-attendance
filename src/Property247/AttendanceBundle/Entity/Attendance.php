<?php

namespace Property247\AttendanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Property247\UserBundle\Entity\User;


/**
 * Attendance
 *
 * @ORM\Table(name="attendance")
 * @ORM\Entity(repositoryClass="Property247\AttendanceBundle\Repository\AttendanceRepository")
 */
class Attendance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="attendance", type="string", length=255)
     */
    private $attendance;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Property247\UserBundle\Entity\User")
     */
    private $student;

    /**
     * @var Subject
     *
     * @ORM\ManyToOne(targetEntity="Property247\AttendanceBundle\Entity\Subject")
     */
    private $subject;


    use CreatedUpdatedOnTrait;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attendance
     *
     * @param string $attendance
     *
     * @return Attendance
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;

        return $this;
    }

    /**
     * Get attendance
     *
     * @return string
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * @return mixed
     */
    public function getStudent()
    {
        return $this->student;
    }



    /**
     * @param User $student
     */
    public function setStudent(User $student)
    {
        $this->student = $student;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }



    /**
     * @param Subject $subject
     */
    public function setSubject(Subject $subject)
    {
        $this->subject = $subject;
    }


}

