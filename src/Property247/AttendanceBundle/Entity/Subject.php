<?php

namespace Property247\AttendanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Property247\UserBundle\Entity\User;


/**
 * Subject
 *
 * @ORM\Table(name="subject")
 * @ORM\Entity(repositoryClass="Property247\AttendanceBundle\Repository\SubjectRepository")
 */
class Subject
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="semester", type="integer")
     */
    private $semester;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Property247\UserBundle\Entity\User")
     */
    private $teacher;

    use CreatedUpdatedOnTrait;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Subject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return Subject
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return int
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * @return mixed
     */
    public function getTeacher()
    {
        return $this->teacher;
    }



    /**
     * @param User $teacher
     */
    public function setTeacher(User $teacher)
    {
        $this->teacher = $teacher;
    }


}

