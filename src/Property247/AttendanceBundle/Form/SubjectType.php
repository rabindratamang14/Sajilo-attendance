<?php

namespace Property247\AttendanceBundle\Form;

use Doctrine\ORM\EntityRepository;
use Property247\UserBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('semester',ChoiceType::class, array(
                'placeholder' => 'Choose an option',
                'choices' => array(
                    '1st Semester'=>1,
                    '2nd Semester'=>2,
                    '3rd Semester'=>3,
                    '4th Semester'=>4,
                    '5th Semester'=>5,
                    '6th Semester'=>6,
                    '7th Semester'=>7,
                    '8th Semester'=>8,
                ),
                'attr'=>array(
                    'class'=>'select2'
                )
            ))
            ->add('teacher',EntityType::class,[
                'class' => User::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose a Teacher',
                'attr'=>[
                    'class'=>'select2'
                ],
                'query_builder'=>function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                                ->andWhere('c.roles LIKE :teacher')
                                ->setParameter('teacher', '%TEACHER%');
                }
            ]);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\AttendanceBundle\Entity\Subject',
            'isUpdating' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_attendancebundle_subject';
    }


}
