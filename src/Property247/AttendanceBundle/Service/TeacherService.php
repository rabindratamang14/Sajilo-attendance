<?php
/**
 * Created by PhpStorm.
 * User: rabindra
 * Date: 9/13/18
 * Time: 10:29 PM
 */

namespace Property247\AttendanceBundle\Service;

use Knp\Component\Pager\Paginator;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManagerInterface;
use Property247\AttendanceBundle\Entity\Subject;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TeacherService
 * @package Property247\AttendanceBundle\Service
 * @DI\Service("property.teacher.service", public=true)
 */
class TeacherService
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var Paginator
     * @DI\Inject("knp_paginator")
     */
    public $paginator;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $requestStack;



    public function getPaginatedSubjectList($filters = [],$user){
        $query = $this->em->getRepository(Subject::class)->getSubjectTeacherListQuery($filters,$user);
        $limit = array_key_exists('limit', $filters) ? $filters['limit'] : 10;
        return $this->paginator->paginate(
            $query,
            $this->requestStack->getCurrentRequest()->get('page', 1),
            $limit
        );
    }
}


