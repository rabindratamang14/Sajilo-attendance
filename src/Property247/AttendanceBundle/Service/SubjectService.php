<?php
/**
 * Created by PhpStorm.
 * User: rabindra
 * Date: 9/13/18
 * Time: 5:34 PM
 */

namespace Property247\AttendanceBundle\Service;

use Knp\Component\Pager\Paginator;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManagerInterface;
use Property247\AttendanceBundle\Entity\Subject;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class SubjectService
 * @package Property247\AttendanceBundle\Service
 * @DI\Service("property.subject.service", public=true)
 */
class SubjectService
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var Paginator
     * @DI\Inject("knp_paginator")
     */
    public $paginator;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $requestStack;


    public function getSubjectList($filters = []){
        $repo = $this->em->getRepository(Subject::class)->getSubjectListQuery($filters);
        return $repo->getQuery()->getResult();
    }

    public function getPaginatedSubjectList($filters = []){
        $query = $this->em->getRepository(Subject::class)->getSubjectListQuery($filters);
        $limit = array_key_exists('limit', $filters) ? $filters['limit'] : 10;
        return $this->paginator->paginate(
            $query,
            $this->requestStack->getCurrentRequest()->get('page', 1),
            $limit
        );
    }

    public function getSubjectById($id){
        return $this->em->getRepository(Subject::class)->find($id);
    }

    public function countSubjects(){
        return $this->em->getRepository(Subject::class)->countSubjects();
    }

}

