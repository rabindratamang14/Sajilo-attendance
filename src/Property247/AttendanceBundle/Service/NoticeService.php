<?php
/**
 * Created by PhpStorm.
 * User: rabindra
 * Date: 9/13/18
 * Time: 8:20 PM
 */

namespace Property247\AttendanceBundle\Service;


use Knp\Component\Pager\Paginator;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManagerInterface;
use Property247\AttendanceBundle\Entity\Notice;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class NoticeService
 * @package Property247\AttendanceBundle\Service
 * @DI\Service("property.notice.service", public=true)
 */
class NoticeService
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var Paginator
     * @DI\Inject("knp_paginator")
     */
    public $paginator;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $requestStack;


    public function getNoticeList($filters = []){
        $repo = $this->em->getRepository(Notice::class)->getNoticeListQuery($filters);
        return $repo->getQuery()->getResult();
    }

    public function getPaginatedNoticeList($filters = []){
        $query = $this->em->getRepository(Notice::class)->getNoticeListQuery($filters);
        $limit = array_key_exists('limit', $filters) ? $filters['limit'] : 10;
        return $this->paginator->paginate(
            $query,
            $this->requestStack->getCurrentRequest()->get('page', 1),
            $limit
        );
    }

    public function getNoticeById($id){
        return $this->em->getRepository(Notice::class)->find($id);
    }

}


