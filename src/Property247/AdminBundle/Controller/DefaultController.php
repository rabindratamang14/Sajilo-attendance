<?php

namespace Property247\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HomeController
 * @package Property247\AdminBundle\Controller
 * @Route("/admin")
 */
class DefaultController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="property_dashboard")
     */
    public function indexAction(Request $request){
        return $this->render('@Property247Admin/Default/index.html.twig');
    }

}
