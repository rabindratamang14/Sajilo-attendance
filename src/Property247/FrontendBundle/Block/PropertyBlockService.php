<?php

namespace Property247\FrontendBundle\Block;

use Doctrine\ORM\EntityManager;
use Property247\FrontendBundle\Service\FrontendService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PropertyBlockService
 * @package Property247\FrontendBundle\Block
 * @DI\Service("property.view_property.block")
 * @DI\Tag(name="sonata.block")
 */

class PropertyBlockService extends AbstractBlockService{

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * @var FrontendService
     */
    private $frontendService;


    /**
     * PropertyBlockService constructor.
     * @param EntityManager $em
     * @param EngineInterface $templating
     * @param FrontendService $frontendService
     *
     * @DI\InjectParams({
     *   "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *   "templating" = @DI\Inject("templating"),
     *    "frontendService" = @DI\Inject("property.frontend.service")
     *
     * })
     */

    public function __construct(EntityManager $em, EngineInterface $templating, FrontendService $frontendService)
    {
        parent::__construct('property.view_property.block',$templating);

        $this->em = $em;
        $this->frontendService = $frontendService;
    }



    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => 'View Properties',
            'template'=>'Property247FrontendBundle:Block:property.html.twig'
        ]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {

       $properties = $this->frontendService->getProperties();

       $results = [];
       $featuredCityResults = [];
       foreach ($properties as $property){

           if($property->getCity()->isFeatured() == true){

               $featuredCityResults[$property->getCity()->getId()]['city'] = $property->getCity();
               $featuredCityResults[$property->getCity()->getId()]['properties'][$property->getPropertyType()][] = $property;
           }else{
               $results[$property->getCity()->getId()]['city'] = $property->getCity();
               $results[$property->getCity()->getId()]['properties'][$property->getPropertyType()][] = $property;
           }

       }

        $settings = $blockContext->getSettings();

        return $this->renderResponse(
            $blockContext->getTemplate(),
            [
                'block'=>$blockContext->getBlock(),
                'settings' =>$settings,
                'cities' =>$results,
                'featuredCities'=>$featuredCityResults
            ],
            $response
        );
    }


}



