<?php

namespace Property247\FrontendBundle\Block;

use Doctrine\ORM\EntityManager;
use Property247\FrontendBundle\Service\FrontendService;
use Property247\PropertyBundle\Entity\Property;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TopRatedPropertyBlock
 * @package Property247\FrontendBundle\Block
 * @DI\Service("property.top_rated_property.block")
 * @DI\Tag(name="sonata.block")
 */
class TopRatedPropertyBlock extends AbstractBlockService{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FrontendService
     */
    private $frontendService;


    /**
     * TopRatedPropertyBlock constructor.
     * @param EntityManager $em
     * @param EngineInterface $templating
     * @param FrontendService $frontendService
     *
     * @DI\InjectParams({
     *   "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *   "templating" = @DI\Inject("templating"),
     *   "frontendService" = @DI\Inject("property.frontend.service")
     *
     * })
     */

    public function __construct(EntityManager $em, EngineInterface $templating,FrontendService $frontendService)
    {
        parent::__construct('property.top_rated_property.block',$templating);

        $this->em = $em;
        $this->frontendService = $frontendService;
    }



    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => 'Top Rated Properties',
            'template'=>'Property247FrontendBundle:Block:top_rated_property.html.twig'
        ]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        $topRatedProperties = $this->frontendService->getTopRatedProperties(7);

        return $this->renderResponse(

            $blockContext->getTemplate(),
            [
                'block'=>$blockContext->getBlock(),
                'settings' =>$settings,
                'topRatedProperties' =>$topRatedProperties
            ],
            $response

        );

    }


}




