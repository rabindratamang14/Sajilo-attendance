<?php

namespace Property247\FrontendBundle\Block;

use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FeaturedPropertyBlockService
 * @package Property247\FrontendBundle\Block
 * @DI\Service("property.featured_property.block")
 * @DI\Tag(name="sonata.block")
 */

class FeaturedPropertyBlockService extends AbstractBlockService{

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * FeaturedPropertyBlockService constructor.
     * @param EntityManager $em
     * @param EngineInterface $templating
     *
     * @DI\InjectParams({
     *   "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *   "templating" = @DI\Inject("templating")
     *
     * })
     */

    public function __construct(EntityManager $em, EngineInterface $templating)
    {
        parent::__construct('property.featured_property.block',$templating);

        $this->em = $em;
    }



    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => 'Featured Properties',
            'template'=>'Property247FrontendBundle:Block:featured_property_location.html.twig'
        ]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        return $this->renderResponse(

            $blockContext->getTemplate(),
            [
                'block'=>$blockContext->getBlock(),
                'settings' =>$settings
            ],
            $response

        );

    }


}




