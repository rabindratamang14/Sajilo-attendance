<?php

namespace Property247\FrontendBundle\Block;

use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SliderBlockService
 * @package Property247\FrontendBundle\Block
 * @DI\Service("property.slider.block")
 * @DI\Tag(name="sonata.block")
 */

class SliderBlockService extends AbstractBlockService{

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * SliderBlockService constructor.
     * @param EntityManager $em
     * @param EngineInterface $templating
     *
     * @DI\InjectParams({
     *   "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *   "templating" = @DI\Inject("templating")
     *
     * })
     */

    public function __construct(EntityManager $em, EngineInterface $templating)
    {
        parent::__construct('property.slider.block',$templating);

         $this->em = $em;
    }



    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => 'Home Slider',
            'template'=>'Property247FrontendBundle:Block:slider.html.twig'
        ]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        return $this->renderResponse(

            $blockContext->getTemplate(),
            [
                'block'=>$blockContext->getBlock(),
                'settings' =>$settings
            ],
            $response

        );

    }


}




