<?php

namespace Property247\FrontendBundle\Controller;

use Doctrine\ORM\EntityManager;
use Property247\PropertyBundle\Constants\Property;
use Property247\PropertyBundle\Service\PropertyService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{

    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @var PropertyService
     * @DI\Inject("property.service")
     */
    private $propertyService;


    /**
     * @Route("/",name="property_frontend_index_page")
     */
    public function indexAction()
    {
        return $this->render('Property247FrontendBundle:Home:indexAttendance.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("property/{id}-{slug}",name="property_frontend_property_detail_page")
     */
    public function propertyDetailAction(Request $request){
        $id = $request->get('id');
        $propertyDetail = $this->propertyService->getPropertyById($id);
        $data['property'] = $propertyDetail;
        return $this->render('Property247FrontendBundle:Detail:property-detail.html.twig',$data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/search", name="property_frontend_search")
     */
    public function propertySearchAction(Request $request){
        $filters = $request->query->all();
        $filters['house'] = Property::DOCUMENT_TYPE_HOUSE;
        $data['properties'] = $this->propertyService->getPaginatedPropertyList($filters);
        return $this->render('@Property247Frontend/Search/search.html.twig', $data);
    }


}
