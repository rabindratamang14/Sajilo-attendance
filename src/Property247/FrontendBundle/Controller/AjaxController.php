<?php

namespace Property247\FrontendBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\PropertyBundle\Entity\Property;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AjaxController extends Controller
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/propertyOverlay/{id}/render", name="property_frontend_ajax_render_property_overlay")
     */
    public function renderPropertyOverlayBlock(Request $request){
        $id = $request->get('id');
        $property = $this->em->getRepository(Property::class)->find($id);
        if($property instanceof Property){
            $template = $this->renderView('@Property247Frontend/Search/overlay-block.html.twig',['property' => $property]);
            return new JsonResponse([
                'success' => true,
                'template' => $template
            ]);
        }
        return new JsonResponse([
            'success' => false,
            'template' => ''
        ]);
    }

}
