<?php

namespace Property247\FrontendBundle\Service;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\PropertyBundle\Entity\Property;


/**
 * Class FrontendService
 * @package Property247\FrontendBundle\Service
 * @DI\Service("property.frontend.service")
 */

 class FrontendService
 {

     /**
      * @var EntityManager
      */
     private $em;


     /**
      * FrontendService constructor
      * @param EntityManager
      *
      * @DI\InjectParams({
      *    "em" = @DI\Inject("doctrine.orm.entity_manager")
      * })
      */

     public function __construct(EntityManager $em)
     {
         $this->em = $em;

     }

     public function getTopRatedProperties($limit){

         return $this->em->getRepository(Property::class)->getTopRatedProperty($limit);

     }


     public function getCityList($featured,$limit){

         return $this->em->getRepository(Property::class)->getAllCities($featured,$limit);


     }

     public function getProperties(){

         return $this->em->getRepository(Property::class)->getAllProperties();


     }



 }