<?php

namespace Property247\UserBundle\Controller;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\UserBundle\Entity\UserGroup;
use Property247\UserBundle\Form\UserGroupType;
use Symfony\Component\HttpFoundation\Request;
use Property247\MainBundle\Annotations\Permissions;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class GroupController
 * @package Property247\UserBundle\Controller
 * @Breadcrumb("User Group",routeName="admin_user_group_list")
 * @Route("/admin/user-group")
 */
class GroupController extends Controller
{
    /**
     * @var EntityManager
     *
     * @DI\Inject("doctrine.orm.default_entity_manager")
     */
    private $em;

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Permissions("list_user_group", group="user_group", route="admin_user_group_list")
     * @Breadcrumb("List")
     * @Route("/list", name="admin_user_group_list")
     */
    public function indexAction()
    {
        $groups = $this->em->getRepository(UserGroup::class)->findAll();

        return $this->render('@Property247User/Group/list.html.twig', compact('groups'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Permissions("add_user_group", group="user_group", desc="Add User Group", parent="list_user_group", route="admin_user_group_add")
     * @Permissions("update_user_group", group="user_group", desc="Update User Group", parent="list_user_group", route="admin_user_group_update")
     * @Route("/add", name="admin_user_group_add")
     * @Route("/{id}/update", name="admin_user_group_update")
     */
    public function addAction(Request $request)
    {
        $data['permittedPermissions'] = [];

        if ($groupId = (int)$request->get('id') and $request->get('_route') == 'admin_user_group_update') {
            $group = $this->em->getRepository(UserGroup::class)->find($groupId);

            if (!$group) {
                throw new NotFoundHttpException;
            }
            $data['permittedPermissions'] = $group->getPermissions();
            $data['isUpdate'] = true;
        } else {
            $group = new UserGroup();
            $data['isUpdate'] = false;
        }
        $form = $this->createForm(UserGroupType::class, $group);



        if ("POST" == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                try {
                    $permissions = $data['permittedPermissions'] = isset($_POST['permissions']) ? $_POST['permissions'] : [];

                    if (count($permissions)) {
                        $group = $form->getData();
                        $group->addRole('ROLE_ADMIN');
                        $group->setPermissions($permissions);
                        $this->em->persist($group);
                        $this->em->flush();
                        $this->addFlash('success', 'Group saved successfully.');

                        return $this->redirectToRoute('admin_user_group_list');
                    } else {
                        $data['errorMessage'] = 'Please choose permissions.';
                    }
                } catch (\Exception $e) {
                    $data['errorMessage'] = $e->getMessage();
                }
            }
        }

        $apy = $this->get('apy_breadcrumb_trail');

        $data['isUpdate'] ? $apy->add($group->getname()) : $apy->add('New');
        $data['form'] = $form->createView();
        $data['permissions'] = $this->get('property.service.permission')->getPermissionsList($group);
        return $this->render('Property247UserBundle:Group:form.html.twig', $data);
    }

}
