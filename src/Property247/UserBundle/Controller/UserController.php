<?php

namespace Property247\UserBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Annotations\Permissions;
use Property247\SettingBundle\Entity\SocialMediaSetting;
use Property247\UserBundle\Entity\User;
use Property247\UserBundle\Form\UserType;
use Property247\UserBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserController
 * @package Property247\UserBundle\Controller
 * @Route("/admin")
 * @Breadcrumb("User", routeName="list_users")
 */
class UserController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var UserService
     * @DI\Inject("property.user.service")
     */
    public $service;

    /**
     * @Route("/users", name="list_users")
     * @Permissions("list_users", group="user", route="list_users")
     * @Breadcrumb("List")
     */
    public function listAction(Request $request){
        $filters = $request->query->all();
        $filters['notAgent'] = true;
        $data['users'] = $this->service->getPaginatedUsers($filters);
        return $this->render('@Property247User/User/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/user/add", name="add_user")
     * @Route("/user/{id}/update", name="update_user")
     * @Permissions("add_user", group="user", parent="list_users", desc="Add User", route="add_user")
     * @Permissions("update_user", group="user", parent="list_users", desc="Update User", route="update_user")
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        $message = "User created.";
        /**
         * @var $apy Trail
         */
        $apy = $this->get('apy_breadcrumb_trail');
        if($id){
            $user = $this->service->getUserById($id);
            if(! $user instanceof User){
                return $this->redirectToRoute('list_users');
            }
            $message = "User updated.";
            $data['isUpdating'] = true;
            $apy->add($user->getUsername());
            $apy->add('Update');
        }   else    {
            $user = new User();
            $apy->add('Create');
        }

        $form = $this->createForm(UserType::class, $user, [
            'user' => $this->getUser(),
            'isUpdating' => $data['isUpdating']
            ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /**
             * @var $user User
             */
            $user = $form->getData();

            if ($data['isUpdating'] == false) {
                $user->setPlainPassword($user->getPassword());
                $user->setUsernameCanonical($user->getUsername());
                $user->setEmailCanonical($user->getEmail());
                $user->setEnabled(true);
                $user->setRoles([]);
                $user->addRole('ROLE_ADMIN');
                $user->setCreatedBy($this->getUser());
            }
            $userManager = $this->get('fos_user.user_manager');

            try{
                $userManager->updateUser($user);
                $this->addFlash('success', $message);
                return $this->redirectToRoute('list_users');
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }

        $data['form'] = $form->createView();
        return $this->render('@Property247User/User/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/user/{id}/change-password", name="user_change_password")
     */
    public function changePasswordAction(Request $request)
    {
        $userId = $request->get('id');
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        if (is_null($user)) {
            throw new NotFoundHttpException();
        }

        $form = $this->createFormBuilder([])
            ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'Password', 'attr' => ['class' => 'form-control']],
                    'second_options' => ['label' => 'Confirm Password', 'attr' => ['class' => 'form-control']],
                    'invalid_message' => 'Password did not match',
                ]
            )
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $formData = $form->getData();
                $user->setPlainPassword($formData['plainPassword']);

                try {
                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($user);
                    $this->addFlash('success', 'Password Changed Successfully.');
                    return $this->redirectToRoute('list_users');
                } catch (\Exception $e) {
                    $data['errorMessage'] = $e->getMessage();
                }
            }
        }

        $data['form'] = $form->createView();
        $data['pageTitle'] = 'User';
        $data['pageDescription'] = 'Change Password for '.$user->getName();

        return $this->render('@Property247User/User/change_password.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/user/{id}/delete", name="delete_user")
     * @Permissions("delete_user", group="user", parent="list_users", desc="Delete User", route="delete_user")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        if($id){
            $user = $this->service->getUserById($id);
            if(! $user instanceof User){
                $user->setEnabled(false);
                $this->em->persist($user);
                try{
                    $this->em->flush();
                    $this->addFlash('success', 'User deleted.');
                }   catch (\Throwable $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirectToRoute('list_users');
    }

}
