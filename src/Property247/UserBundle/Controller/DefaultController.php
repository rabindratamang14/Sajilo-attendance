<?php

namespace Property247\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class HomeController
 * @package Property247\UserBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @Route("/admin", name="admin_home_route")
     */
    public function adminAction(Request $request){
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $data = [
            'user' => $this->getUser(),
            'type' => 'ADMIN'
        ];
        return $this->render('Property247UserBundle:Default:index.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/agent", name="agent_home_route")
     */
    public function agentAction(Request $request){
        $this->denyAccessUnlessGranted('ROLE_AGENT');
        $data = [
            'user' => $this->getUser(),
            'type' => 'AGENT'
        ];
        return $this->render('Property247UserBundle:Default:index.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/student", name="student_home_route")
     */
    public function studentAction(Request $request){
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        $data = [
            'user' => $this->getUser(),
            'type' => 'STUDENT'
        ];
        return $this->render('Property247UserBundle:Default:index.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/teacher", name="teacher_home_route")
     */
    public function teacherAction(Request $request){
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        $data = [
            'user' => $this->getUser(),
            'type' => 'TEACHER'
        ];
        return $this->render('Property247UserBundle:Default:index.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/user", name="user_home_route")
     */
    public function userAction(Request $request){
        $this->denyAccessUnlessGranted('ROLE_USER');
        $data = [
            'user' => $this->getUser(),
            'type' => 'USER'
        ];
        return $this->render('Property247UserBundle:Default:index.html.twig', $data);
    }

}
