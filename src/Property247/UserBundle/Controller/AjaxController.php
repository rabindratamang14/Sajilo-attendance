<?php

namespace Property247\UserBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Property247\UserBundle\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class AjaxController
 * @package Property247\UserBundle\Controller
 * @Route("/admin")
 */
class AjaxController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/agent-group/{id}/set", name="ajax_set_default_agent_user_group")
     */
    public function setAgentDefaultGroup(Request $request){
        $id = $request->get('id');
        if($id){
            $userGroup = $this->em->getRepository(UserGroup::class)->find($id);
            if(! $userGroup instanceof UserGroup){
                $data = [
                    'success' => false,
                    'message' => 'User group does not exist.'
                ];
                return new JsonResponse($data);
            }
        }   else    {
            $data = [
                'success' => false,
                'message' => 'User group does not exist.'
            ];
            return new JsonResponse($data);
        }

        $userGroups = $this->em->getRepository(UserGroup::class)->findAll();

        /**
         * @var $ug UserGroup
         */
        foreach ($userGroups as $ug){
            $ug->setIsDefaultAgentGroup(false);
            $this->em->persist($ug);
        }

        $userGroup->setIsDefaultAgentGroup(true);
        $this->em->persist($userGroup);

        try{
            $this->em->flush();
            $data = [
                'success' => true,
                'message' => 'Default agent User group set.'
            ];
        }   catch (\Throwable $e){
            $data = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        return new JsonResponse($data);
    }

}
