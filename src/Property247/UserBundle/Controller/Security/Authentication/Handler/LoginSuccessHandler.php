<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/11/18
 * Time: 8:25 AM
 */

namespace Property247\UserBundle\Controller\Security\Authentication\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class LoginSuccessHandler
 * @package Property247\UserBundle\Controller\Security\Authentication\Handler
 * @DI\Service("login_success_handler")
 */
class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    /**
     * @var Security
     * @DI\Inject("security.helper")
     */
    public $security;

    /**
     * @var Router
     * @DI\Inject("router")
     */
    public $router;

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // URL for redirect the user to where they were before the login process begun if you want.
        // $referer_url = $request->headers->get('referer');

        // Default target for unknown roles. Everyone else go there.
        $url = 'agent_home_route';

        if($this->security->isGranted('ROLE_ADMIN')) {
            $url = 'admin_home_route';
            return new RedirectResponse($this->router->generate($url));
        }
        if($this->security->isGranted('ROLE_AGENT')) {
            $url = 'agent_home_route';
            return new RedirectResponse($this->router->generate($url));
        }
        if($this->security->isGranted('ROLE_STUDENT')) {
            $url = 'student_home_route';
            return new RedirectResponse($this->router->generate($url));
        }
        if($this->security->isGranted('ROLE_TEACHER')) {
            $url = 'teacher_home_route';
            return new RedirectResponse($this->router->generate($url));
        }

        $response = new RedirectResponse($this->router->generate($url));
        return $response;
    }

}