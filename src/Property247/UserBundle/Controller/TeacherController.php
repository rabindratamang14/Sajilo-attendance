<?php

namespace Property247\UserBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Annotations\Permissions;
use Property247\SettingBundle\Entity\SocialMediaSetting;
use Property247\UserBundle\Entity\User;
use Property247\UserBundle\Entity\UserGroup;
use Property247\UserBundle\Form\UserType;
use Property247\UserBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StudentController
 * @package Property247\UserBundle\Controller
 * @Route("/admin/teacher")
 */
class TeacherController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var UserService
     * @DI\Inject("property.user.service")
     */
    protected $service;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_teacher_list")
     */
    public function listAction(Request $request){
        $filters = $request->query->all();
        $filters['role'] = 'TEACHER';
        $user = $this->getUser();

        if(in_array('ROLE_TEACHER', $user->getRoles()) or in_array('ROLE_TEACHER', $user->getRoles())){
            $filters['createdBy'] = $user->getId();
        }

        $data['users'] = $this->service->getPaginatedUsers($filters);
        return $this->render('@Property247User/Teacher/list.html.twig', $data);
    }
    /**
     * @param Request $request
     * @Route("/add", name="admin_teacher_create")
     * @Route("/{id}/update", name="admin_teacher_update")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addStudent(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        $message = "User created.";
        $data['isTeacher']= true;
        /**
         * @var $apy Trail
         */
        $apy = $this->get('apy_breadcrumb_trail');
        if($id){
            $user = $this->service->getUserById($id);
            if(! $user instanceof User){
                return $this->redirectToRoute('list_users');
            }
            $message = "User updated.";
            $data['isUpdating'] = true;
            $apy->add($user->getUsername());
            $apy->add('Update');
        }   else    {
            $user = new User();
            $apy->add('Create');
        }

        $form = $this->createForm(UserType::class, $user, [
            'user' => $this->getUser(),
            'isUpdating' => $data['isUpdating'],
            'isTeacher'=>$data['isTeacher']
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /**
             * @var $user User
             */
            $user = $form->getData();

            if ($data['isUpdating'] == false) {
                $user->setPlainPassword($user->getPassword());
                $user->setUsernameCanonical($user->getUsername());
                $user->setEmailCanonical($user->getEmail());
                $user->setEnabled(true);
                $user->setRoles([]);
                $user->addRole('ROLE_TEACHER');
                $currentUserRoles = $this->getUser()->getRoles();

//                if(in_array('ROLE_SUPER_ADMIN', $currentUserRoles)){
//                    $user->addRole('ROLE_AGENT_ADMIN');
//                }

                $user->setCreatedBy($this->getUser());
            }
            $userManager = $this->get('fos_user.user_manager');

            try{
                $this->em->flush();
                $userManager->updateUser($user);
                $this->addFlash('success', $message);
                return $this->redirectToRoute('admin_teacher_list');
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }

        $data['form'] = $form->createView();
        return $this->render('@Property247User/Teacher/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @Route("/{id}/delete", name="admin_teacher_delete")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteStudent(Request $request){
        $filters = $request->query->all();
        $filters['role'] = 'TEACHER';
        $agent = $this->em->getRepository(User::class)->getUserListQuery($filters);
        if($agent instanceof User){
            $agent->setEnabled(false);
            $this->em->persist($agent);
            try{
                $this->em->flush();
                $this->addFlash('success', 'Agent deleted.');
            } catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->redirectToRoute('admin_teacher_list');
    }
}
