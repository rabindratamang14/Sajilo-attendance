<?php

namespace Property247\UserBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Annotations\Permissions;
use Property247\SettingBundle\Entity\SocialMediaSetting;
use Property247\UserBundle\Entity\User;
use Property247\UserBundle\Entity\UserGroup;
use Property247\UserBundle\Form\UserType;
use Property247\UserBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgentController
 * @package Property247\UserBundle\Controller
 * @Route("/admin/agent")
 * @Breadcrumb("agent", routeName="admin_agent_list")
 */
class AgentController extends Controller
{
    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var UserService
     * @DI\Inject("property.user.service")
     */
    protected $service;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_agent_list")
     * @Permissions("list_agents", group="agent")
     */
    public function listAction(Request $request){
        $filters = $request->query->all();
        $filters['role'] = 'AGENT';
        $user = $this->getUser();

        if(in_array('ROLE_AGENT', $user->getRoles()) or in_array('ROLE_AGENT_ADMIN', $user->getRoles())){
            $filters['createdBy'] = $user->getId();
        }

        $data['users'] = $this->service->getPaginatedUsers($filters);
        return $this->render('@Property247User/Agent/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @Route("/add", name="admin_agent_create")
     * @Route("/{id}/update", name="admin_agent_update")
     * @Permissions("add_agent", group="agent", desc="Add agent", route="admin_agent_create")
     * @Permissions("update_agent", group="agent", desc="Update agent", route="admin_agent_update")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        $message = "User created.";
        $data['agentDefaultGroupSet'] = false;
        $agentDefaultGroup = $this->em->getRepository(UserGroup::class)->findOneBy([
            'isDefaultAgentGroup' => true
        ]);

        if(! $agentDefaultGroup instanceof UserGroup){
            $data['agentDefaultGroupSet'] = true;
        }

        /**
         * @var $apy Trail
         */
        $apy = $this->get('apy_breadcrumb_trail');
        if($id){
            $user = $this->service->getUserById($id);
            if(! $user instanceof User){
                return $this->redirectToRoute('list_users');
            }
            $message = "User updated.";
            $data['isUpdating'] = true;
            $apy->add($user->getUsername());
            $apy->add('Update');
        }   else    {
            $user = new User();
            $apy->add('Create');
        }

        $form = $this->createForm(UserType::class, $user, [
            'user' => $this->getUser(),
            'isUpdating' => $data['isUpdating']
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /**
             * @var $user User
             */
            $user = $form->getData();

            if($agentDefaultGroup instanceof UserGroup){
                $user->addUserGroup($agentDefaultGroup);
            }

            if ($data['isUpdating'] == false) {
                $user->setPlainPassword($user->getPassword());
                $user->setUsernameCanonical($user->getUsername());
                $user->setEmailCanonical($user->getEmail());
                $user->setEnabled(true);
                $user->setRoles([]);
                $user->addRole('ROLE_AGENT');
                $currentUserRoles = $this->getUser()->getRoles();

                if(in_array('ROLE_SUPER_ADMIN', $currentUserRoles)){
                    $user->addRole('ROLE_AGENT_ADMIN');
                }

                $user->setCreatedBy($this->getUser());
            }
            $userManager = $this->get('fos_user.user_manager');

            try{
                $this->em->flush();
                $userManager->updateUser($user);
                $this->addFlash('success', $message);
                return $this->redirectToRoute('admin_agent_list');
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }

        $data['form'] = $form->createView();
        return $this->render('@Property247User/Agent/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @Route("/{id}/delete", name="admin_agent_delete")
     * @Permissions("delete_agent", group="agent", desc="Delete agent", route="admin_agent_delete")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAgent(Request $request){
        $filters = $request->query->all();
        $filters['role'] = 'AGENT';
        $agent = $this->em->getRepository(User::class)->getUserListQuery($filters);
        if($agent instanceof User){
            $agent->setEnabled(false);
            $this->em->persist($agent);
            try{
                $this->em->flush();
                $this->addFlash('success', 'Agent deleted.');
            } catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->redirectToRoute('admin_list_agent');
    }

}
