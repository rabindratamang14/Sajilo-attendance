<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/22/18
 * Time: 3:12 PM
 */

namespace Property247\UserBundle\Service;

use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Service\MainService;
use Property247\UserBundle\Entity\User;

/**
 * Class UserService
 * @package Property247\UserBundle\Service
 * @DI\Service("property.user.service", public=true)
 */
class UserService extends MainService
{

    public function getUserById($id){
        return $this->em->getRepository(User::class)->find($id);
    }

    public function getPaginatedUsers($filters = []){
        $qb = $this->em->getRepository(User::class)->getUserListQuery($filters);
        return $this->paginate($qb);
    }

    public function countStudent(){
        return $this->em->getRepository(User::class)->countStudent();
    }
    public function countTeacher(){
        return $this->em->getRepository(User::class)->countTeacher();
    }

}