<?php

namespace Property247\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * UserGroup
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="Property247\UserBundle\Repository\UserGroupRepository")
 */
class UserGroup extends BaseGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var array
     *
     * @ORM\Column(name="permissions", type="array", nullable=true )
     */
    private $permissions;

    /**
     * @var \Doctrine\Common\Collections\Collection|User[]
     *
     * @ORM\ManyToMany(targetEntity="Property247\UserBundle\Entity\User", mappedBy="userGroups", cascade={"persist"})
     */
    protected $users;

    /**
     * @var boolean
     * @ORM\Column(name="is_default_agent_group", type="boolean")
     */
    protected $isDefaultAgentGroup = false;

    /**
     * UserGroup constructor.
     */
    public function __construct()
    {
        parent::__construct('', []);

        $this->users = new ArrayCollection();
    }

    public function __toString() :string
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param array $permissions
     *
     * @return UserGroup
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return UserGroup
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addGroup($this);
        }

        return $this;
    }

    /**
     * @param User $user
     *
     * @return UserGroup
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeGroup($this);
        }

        return $this;

    }

    /**
     * @return bool
     */
    public function isDefaultAgentGroup(): bool
    {
        return $this->isDefaultAgentGroup;
    }

    /**
     * @param bool $isDefaultAgentGroup
     */
    public function setIsDefaultAgentGroup(bool $isDefaultAgentGroup)
    {
        $this->isDefaultAgentGroup = $isDefaultAgentGroup;
    }

}

