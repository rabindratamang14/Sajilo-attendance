<?php

namespace Property247\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \FOS\UserBundle\Model\User as BaseUser;
use Property247\SettingBundle\Entity\SocialMediaSetting;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Property247\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=50, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var int
     *
     * @ORM\Column(name="roll", nullable=true)
     */
    private $roll;

    /**
     * @var int
     *
     * @ORM\Column(name="semester", nullable=true)
     */
    private $semester;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Property247\UserBundle\Entity\UserGroup", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="ys_user_groups_relation",
     *      joinColumns={ @ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={ @ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    private $userGroups;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Property247\UserBundle\Entity\User")
     */
    private $createdBy;

    public function __construct()
    {

        parent::__construct();
        $this->userGroups = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserGroups()
    {
        return $this->userGroups;
    }

    /**
     * @param UserGroup $userGroup
     *
     * @return User
     */
    public function addUserGroup(UserGroup $userGroup)
    {
        if (!$this->userGroups->contains($userGroup)) {
            $userGroup->addUser($this);
            $this->userGroups->add($userGroup);
        }


        return $this;
    }

    /**
     * @param UserGroup $userGroup
     *
     * @return User
     */
    public function removeUserGroup(UserGroup $userGroup)
    {
        if ($this->userGroups->contains($userGroup)) {
            $this->userGroups->removeElement($userGroup);
            $userGroup->removeUser($this);
        }

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return int
     */
    public function getRoll()
    {
        return $this->roll;
    }

    /**
     * @param int $roll
     */
    public function setRoll($roll)
    {
        $this->roll = $roll;
    }

    /**
     * @return int
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * @param int $semester
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;
    }



}

