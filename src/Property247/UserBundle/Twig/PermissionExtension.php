<?php

namespace Property247\UserBundle\Twig;

use Twig_Extension;
use Symfony\Component\Routing\Router;
use JMS\DiExtraBundle\Annotation as DI;
use Property247\MainBundle\Service\PermissionService;

/**
 * Class PermissionExtension
 * @package YarshaStudio\UserBundle\Twig
 *
 * @DI\Service("property.twig.permission")
 * @DI\Tag(name="twig.extension")
 */
class PermissionExtension extends Twig_Extension
{
    /**
     * @var PermissionService
     */
    private $permissionService;

    /**
     * @var Router
     */
    private $router;

    /**
     * PermissionService constructor.
     *
     * @DI\InjectParams({
     *     "permissionService" = @DI\Inject("property.service.permission"),
     *     "router" = @DI\Inject("router"),
     *     })
     */
    public function __construct(PermissionService $permissionService, Router $router)
    {
        $this->permissionService = $permissionService;
        $this->router = $router;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('has_access', [$this, 'hasAccess']),
            new \Twig_SimpleFunction('render_menu', [$this, 'renderMenu'], ['is_safe' => ['html']]),
        ];
    }

    public function hasAccess($permission)
    {
        return $this->permissionService->hasPermission($permission);
    }

    public function renderMenu($permission, $route, $label, $currentPath)
    {
        $class = '';
        if ($currentPath == $route) {
            $class = 'class="active"';
        }
        $path = $this->router->generate($route);

        return ($this->hasAccess($permission))
            ? "<li ".$class."><a href=\"$path\">$label</a></li>"
            : "";
    }

    public function getName()
    {
        return 'permission_twig';
    }
}