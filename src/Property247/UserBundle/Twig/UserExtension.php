<?php

namespace YarshaStudio\UserBundle\Twig;

use Doctrine\ORM\EntityManager;
use Property247\UserBundle\Entity\User;

/**
 * Class UserExtension
 */
class UserExtension extends \Twig_Extension
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * UserExtension constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'user_select',
                [$this, 'userSelect'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    public function userSelect($name, $selected = null, $attr = '')
    {
        $users = $this->em->getRepository(User::class)->findAll();
        $html = '<select name="'.$name.'" '.$attr.'>';
        $html .= '<option value="">-- Any User --</option>';

        if (count($users)) {
            foreach ($users as $user) {
                $selectedString = ($selected == ($userId = $user->getId())) ? 'selected="selected"' : '';
                $html .= '<option value="'.$userId.'" '.$selectedString.'> '.$user->getName().' </option>';
            }

        }

        $html .= '</select>';

        return $html;
    }

    public function getName()
    {
        return 'user_extension';
    }

}
