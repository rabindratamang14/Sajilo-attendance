<?php

namespace Property247\UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Property247\SettingBundle\Entity\SocialMediaSetting;
use Property247\SettingBundle\Form\SocialMediaSettingType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];

        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('phone')
            ->add('mobile')
            ->add('address')
            ->add('email');
            if(!$options['isTeacher']){
              $builder->add('roll', NumberType::class, ['required' => true])
                    ->add('semester',ChoiceType::class, array(
                        'placeholder' => 'Choose an option',
                        'choices' => array(
                            '1st Semester'=>1,
                            '2nd Semester'=>2,
                            '3rd Semester'=>3,
                            '4th Semester'=>4,
                            '5th Semester'=>5,
                            '6th Semester'=>6,
                            '7th Semester'=>7,
                            '8th Semester'=>8,
                        ),
                        'attr'=>array(
                            'class'=>'select2'
                        )
                    ));
            }


            /*if(! $options['isAgent']){
                $builder->add('userGroups', null, [
                    'query_builder' => function (EntityRepository $er) use ($user) {
                        $qb = $er->createQueryBuilder('g');

                        if (!$user->hasRole('ROLE_SUPER_ADMIN')) {
                            $qb->leftJoin('g.users', 'u')
                                ->where('u.id = :userId')->setParameter('userId', $user->getId());
                        }

                        return $qb;
                    },
                    'attr' => [
                        'class' => 'select2',
                    ],
                ]);
            }*/

        if ($options['isUpdating'] === false) {//{{ form_row(form.userGroups) }}

            $builder
                ->add('username', null, [
                    'label' => 'Username',
                    'required' => true,
                ])
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Password fields must match.',
                    'first_options' => [
                        'label' => 'Password',
                    ],
                    'second_options' => [
                        'label' => 'Confirm Password',
                    ],
                ]);
        }
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Property247\UserBundle\Entity\User',
            'user' => null,
            'isUpdating' => true,
            'socialMediaSetting' => null,
            'isAgent' => false,
            'isTeacher'=>false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'property247_userbundle_user';
    }


}
