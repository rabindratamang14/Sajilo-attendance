<?php

namespace YarshaStudio\LocalityBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use YarshaStudio\LocalityBundle\Entity\City;
use YarshaStudio\LocalityBundle\Entity\District;
use YarshaStudio\LocalityBundle\Entity\Province;

class CityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('type', ChoiceType::class, [
                'placeholder' => 'Select Type',
                'choices' => array_flip(City::$typeArray)
            ])
            ->add('featured')
            ->add('image', VichImageType::class, [
                'required' => false,
                'label' => 'Featured Image'
            ])
            ->add('province', EntityType::class, [
                'class' => Province::class,
                'query_builder' => function (EntityRepository $e) {
                    return $e->createQueryBuilder('p')
                        ->where('p.deleted = :deleted')
                        ->setParameter('deleted', false);
                },
                'attr' => [
                    'class' => 'select-province'
                ],
                'placeholder' => 'Select Province'
            ])
            ->add('district', EntityType::class, [
                'class' => District::class,
                'query_builder' => function (EntityRepository $e) {
                    return $e->createQueryBuilder('d')
                        ->where('d.deleted = :deleted')
                        ->setParameter('deleted', false);
                },
                'attr' => [
                    'class' => 'district-select'
                ],
                'choice_attr' => function (District $district) {
                    return [
                        'class' => 'select-district',
                        'data-province-id' => $district->getProvince() ? $district->getProvince()->getId() : 0
                    ];
                },
                'placeholder' => 'Select District'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'YarshaStudio\LocalityBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'yarshastudio_localitybundle_city';
    }


}
