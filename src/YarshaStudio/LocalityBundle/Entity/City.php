<?php

namespace YarshaStudio\LocalityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="YarshaStudio\LocalityBundle\Repository\CityRepository")
 * @Vich\Uploadable
 */
class City
{
    const VDC = '001';
    const MUNICIPALITY = '002';
    const SUB_METROPOLITAN_CITY = '003';
    const METROPOLITAN_CITY = '004';

    public static $typeArray = [
        self::VDC => 'Vdc',
        self::MUNICIPALITY => 'Municipality',
        self::SUB_METROPOLITAN_CITY => 'Sub Metropolitan City',
        self::METROPOLITAN_CITY => 'Metropolitan City'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @var string
    * @ORM\Column(name="name", type="string")
    */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
    * @var bool
    * @ORM\Column(name="featured", type="boolean")
    */
    private $featured = 0;

    /**
     * @var Province
     * @ORM\ManyToOne(targetEntity="YarshaStudio\LocalityBundle\Entity\Province")
     */
    private $province;

    /**
     * @var District
     * @ORM\ManyToOne(targetEntity="YarshaStudio\LocalityBundle\Entity\District")
     */
    private $district;

    /**
     * @Assert\File(mimeTypes={"image/*"})
     * @Vich\UploadableField(mapping="city_image", fileNameProperty="imageName")
     * @var File
     */
    private $image;

    /**
     * @var string
     * @ORM\Column(name="image_name", type="string", nullable=true)
     */
    private $imageName;

    use CreatedUpdatedOnTrait;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured)
    {
        $this->featured = $featured;
    }

    /**
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param Province $province
     */
    public function setProvince(Province $province)
    {
        $this->province = $province;
    }

    /**
     * @return District
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param District $district
     */
    public function setDistrict(District $district)
    {
        $this->district = $district;
    }

    /**
     * @return UploadedFile
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param File $image
     */
    public function setImage(File $image = null)
    {
        if($image instanceof File){
            $this->image = $image;
        }
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    public function __toString()
    {
        return $this->name ? $this->name : 'City';
    }

}

