<?php

namespace YarshaStudio\LocalityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;

/**
 * District
 *
 * @ORM\Table(name="ys_district")
 * @ORM\Entity(repositoryClass="YarshaStudio\LocalityBundle\Repository\DistrictRepository")
 */
class District
{
    use CreatedUpdatedOnTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="YarshaStudio\LocalityBundle\Entity\LocalityRegion")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=255)
     */
    private $nameEn;

    /**
     * @var Province
     * @ORM\ManyToOne(targetEntity="YarshaStudio\LocalityBundle\Entity\Province", cascade={"persist"})
     */
    private $province;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return District
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * @param $nameEn
     *
     * @return District
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param Province $province
     * @return District
     */
    public function setProvince(Province $province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nameEn ? $this->nameEn : 'district';
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

}

