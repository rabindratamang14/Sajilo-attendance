<?php

namespace YarshaStudio\LocalityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Property247\MainBundle\Traits\CreatedUpdatedOnTrait;

/**
 * Province
 *
 * @ORM\Table(name="ys_province")
 * @ORM\Entity(repositoryClass="YarshaStudio\LocalityBundle\Repository\ProvinceRepository")
 */
class Province
{
    use CreatedUpdatedOnTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="YarshaStudio\LocalityBundle\Entity\District")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="province_name", type="string", length=255)
     */
    private $provinceName;

    /**
     * @var string
     *
     * @ORM\Column(name="province_name_np", type="string", length=255)
     */
    private $provinceNameNp;
    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="YarshaStudio\LocalityBundle\Entity\Country", cascade={"persist"})
     */
    private $country;

    private $label;

    public function setLabel($label){
        $this->label = $label;
    }

    public function getLabel(){
        return $this->label ? $this->label : $this->provinceName;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProvinceName()
    {
        return $this->provinceName;
    }

    /**
     * @param string $provinceName
     */
    public function setProvinceName($provinceName)
    {
        $this->provinceName = $provinceName;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return $this;
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->provinceName ? $this->provinceName : 'province name';
    }

    /**
     * @return string
     */
    public function getProvinceNameNp()
    {
        return $this->provinceNameNp;
    }

    /**
     * @param string $provinceNameNp
     */
    public function setProvinceNameNp($provinceNameNp)
    {
        $this->provinceNameNp = $provinceNameNp;
    }

}

