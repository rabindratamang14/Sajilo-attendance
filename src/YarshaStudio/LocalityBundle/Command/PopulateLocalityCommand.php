<?php


namespace YarshaStudio\LocalityBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use YarshaStudio\LocalityBundle\Entity\Country;
use YarshaStudio\LocalityBundle\Entity\District;
use YarshaStudio\LocalityBundle\Entity\Province;

class PopulateLocalityCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('yarsha:populate:locality')
            ->setDescription('Sets Province, districts and municipality');

    }

    private function getNepaliNumber($num)
    {
        $str = [];
        $numarr = str_split($num);
        if (count($numarr) == 1) {
            array_unshift($numarr, '0');
        }
        $number = ['०', '१', '२', '३', '४', '५', '६', '७', '८', '९'];
        for ($i = 0; $i < count($numarr); $i++) {
            $str[$i] = $number[$numarr[$i]];
        }

        return implode('', $str);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $locationJsonFile = $this->getContainer()->get('kernel')->getRootDir().'/../web/province.json';
        $jsonData = file_get_contents($locationJsonFile);
        $locationInArray = json_decode($jsonData);
        $country = new Country();
        $country->setName('Nepal')
            ->setIso2('NP')
            ->setIso3('NPL')
            ->setDeleted(false);
        $em->persist($country);

        foreach ($locationInArray as $province => $value) {
            $provinceName = 'Province '.$province;
            $provinceNepaliName = 'प्रदेश '.$this->getNepaliNumber($province);
            $formatedprovinceNepaliName = str_replace("०","",$provinceNepaliName);
            $province = new Province();
            $province->setCountry($country);
            $province->setProvinceNameNp($formatedprovinceNepaliName);
            $province->setProvinceName($provinceName);
            $em->persist($province);
            foreach ($value as $location) {
                $district = new District();
                $district->setNameEn(ucwords($location->english_name))
                    ->setName($location->nepali_name)
                    ->setProvince($province);
                $em->persist($district);
            }
        }
        try {
            $em->flush();
            $output->writeln('Locations Populated Successfully!');
        } catch (\Throwable $t) {
            dd($t->getMessage());
        }

    }
}
