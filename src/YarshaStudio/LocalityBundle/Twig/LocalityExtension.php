<?php
/**
 * Created by PhpStorm.
 * User: mandip
 * Date: 6/21/18
 * Time: 4:51 PM
 */

namespace YarshaStudio\LocalityBundle\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use JMS\DiExtraBundle\Annotation as DI;
use YarshaStudio\LocalityBundle\Entity\Province;

/**
 * Class LocalityExtension
 * @package YarshaStudio\LocalityBundle\Twig
 * @DI\Service("locality.extension", public=true)
 * @DI\Tag("twig.extension")
 */
class LocalityExtension extends AbstractExtension
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    public function getFunctions(){
        return [
            new \Twig_SimpleFunction('render_province_filter',[$this,'renderProvinceFilter'],['is_safe' => ['html']])
        ];
    }

    public function renderProvinceFilter($province = ''){
        $provinces = $this->em->getRepository(Province::class)->getAllProvince([]);
        $html = "<select name='province' class='form-control'>";
        $html .= "<option value=''>Select province</option>";

        /**
         * @var $p Province
         */
        foreach ($provinces as $p){
            $selected = $p->getId() == $province ? 'selected' : '';
            $html .= "<option value='{$p->getId()}' {$selected}>{$p->getProvinceNameNp()}</option>";
        }
        $html .= "</select>";
        return $html;
    }

}