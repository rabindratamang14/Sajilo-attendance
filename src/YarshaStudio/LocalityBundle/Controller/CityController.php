<?php

namespace YarshaStudio\LocalityBundle\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use YarshaStudio\LocalityBundle\Entity\City;
use YarshaStudio\LocalityBundle\Form\CityType;

/**
 * Class CityController
 * @package YarshaStudio\LocalityBundle\Controller
 * @Route("/admin")
 */
class CityController extends Controller
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var Trail
     * @DI\Inject("apy_breadcrumb_trail")
     */
    public $apy;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/city", name="yarsha_admin_city_list")
     * @Breadcrumb("City", routeName="yarsha_admin_city_list")
     * @Breadcrumb("List")
     */
    public function listAction(Request $request){
        $data['cities'] = $this->em->getRepository(City::class)->findBy([
            'deleted' => false
        ]);
        return $this->render('@YarshaStudioLocality/city/list.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/city/add",name="yarsha_admin_city_add")
     * @Route("/city/{id}/update",name="yarsha_admin_city_update")
     * @Breadcrumb("City", routeName="yarsha_admin_city_list")
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $data['isUpdating'] = false;
        if($id){
            $city = $this->em->getRepository(City::class)->find($id);
            if(! $city instanceof City){
                return $this->redirectToRoute('yarsha_admin_city_add');
            }
            $this->apy->add($city->getName());
            $this->apy->add('Update');
            $message = "City updated.";
            $data['isUpdating'] = true;
        }   else    {
            $message = "City added.";
            $city = new City();
            $this->apy->add('Create');
        }

        $form = $this->createForm(CityType::class, $city);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /**
             * @var City
             */
            $formData = $form->getData();
            $this->em->persist($formData);

            try{
                $this->em->flush();
                $this->addFlash('success', $message);
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirectToRoute('yarsha_admin_city_list');
        }
        $data['form'] = $form->createView();
        return $this->render('@YarshaStudioLocality/city/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/city/{id}/delete", name="yarsha_admin_city_delete")
     */
    public function deleteAction(Request $request){
        $id = $request->get('id');
        $city = $this->em->getRepository(City::class)->find($id);
        if($city instanceof City){
            $city->setDeleted(true);
            $this->em->persist($city);
            try{
                $this->em->flush();
                $this->addFlash('success', 'City deleted');
            }   catch (\Throwable $e){
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->redirectToRoute('yarsha_admin_city_list');
    }

}
