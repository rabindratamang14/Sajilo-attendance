<?php

namespace YarshaStudio\LocalityBundle\Controller;

use APY\BreadcrumbTrailBundle\APYBreadcrumbTrailBundle;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use YarshaStudio\LocalityBundle\Entity\District;
use YarshaStudio\LocalityBundle\Entity\Province;

/**
 * Class LocalityController
 * @package YarshaStudio\LocalityBundle\Controller
 * @Route("/admin")
 */
class LocalityController extends Controller
{

    /**
     * @var EntityManagerInterface
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/locality/province", name="yarsha_province_list")
     * @Breadcrumb("Province")
     * @Breadcrumb("List")
     */
    public function listProvinceAction(Request $request){
        $filters = $request->query->all();
        $data['provinces'] = $this->em->getRepository(Province::class)->getAllProvince($filters);
        return $this->render('@YarshaStudioLocality/provinceList.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/locality/district", name="yarsha_district_list")
     * @Breadcrumb("District")
     * @Breadcrumb("List")
     */
    public function listDistrictAction(Request $request){
        $filters = $request->query->all();
        $data['districts'] = $this->em->getRepository(District::class)->getAllDistrictsQuery($filters)->getQuery()->getResult();
        return $this->render('@YarshaStudioLocality/districtList.html.twig', $data);
    }

}
