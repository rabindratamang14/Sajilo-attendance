var modal = $('#medialibrary');
var modal = $('#medialibrary');

ckeditorImageInsertBtnDiv = "<div id='ckeditorImageInsertBtnDiv'></div>";
insertImageBtnDiv = "<div id='insertImageBtnDiv'></div>";

modal.find('div.modal-footer').append(insertImageBtnDiv);

$('.addmedia').on('click', function(){
    modal.find('button#insertMediaCkeditor').css('display', 'block');
    modal.find('button#insertImageBtn').css('display', 'none');
});

function insertImageUrl(obj, imagePreviewDivClass){
    modal.find('button#insertMediaCkeditor').css('display', 'none');
    modal.find('button#insertImageBtn').css('display', 'block');
    insertBtn = "<button class='btn btn-primary pull-right' id='insertImageBtn'>Insert Image</button>";
    modal.find('div#insertImageBtnDiv').html(insertBtn);
    modal.modal('toggle');
    $('#insertImageBtn').on('click', function(){
        imageUrl = modal.find('input#url').val();
        $(obj).val(imageUrl);
        previewImage(imageUrl, obj, imagePreviewDivClass);
        modal.modal('toggle');
    });
}

function previewImage(imageUrl, obj, imagePreviewDivClass){
    previewImageDiv = $(obj).parent().parent().parent();
    image = "<img style='height: auto;width:100%;' src='"+imageUrl+"'>";
    $(previewImageDiv).find('.'+imagePreviewDivClass+'Holder').html(image);
    removeBtn = '<a class="removeImage pull-right" title="Remove Image"><i class="fa fa-trash"></i></a><div class="clearfix"></div>';
    $('.'+imagePreviewDivClass+'Holder').append(removeBtn);
    $('.removeImage').on('click', function(e){
        e.preventDefault();
        $('.'+imagePreviewDivClass+'Holder').find('img').remove();
        $('.'+imagePreviewDivClass+' ').val('');
        $(this).remove();
    });
}