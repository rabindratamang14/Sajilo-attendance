imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: settings.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en

framework:
    #esi: ~
    #translator: { fallbacks: ['%locale%'] }
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    templating:
        engines: ["twig"]
    #serializer: { enable_annotations: true }
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        # https://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id: session.handler.native_file
        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    form_themes:
        - '@Property247Admin/Form/admin_form_horizontal_layout.html.twig'
        -  'VichUploaderBundle:Form:fields.html.twig'

# Doctrine Configuration
doctrine:
    dbal:
        driver: pdo_mysql
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: '%kernel.project_dir%/var/data/data.sqlite'
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #path: '%database_path%'

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: '%mailer_transport%'
    host: '%mailer_host%'
    username: '%mailer_user%'
    password: '%mailer_password%'
#    spool: { type: memory }

fos_user:
    db_driver: orm
    user_class: Property247\UserBundle\Entity\User
    firewall_name: main
    service:
        mailer: fos_user.mailer.twig_swift
    from_email:
        address: "%mailer_user%"
        sender_name: "%mailer_user%"

jms_di_extra:
    locations:
        all_bundles: true
        directories: ["%kernel.root_dir%/../src"]

stof_doctrine_extensions:
    default_locale: en_US
    orm:
        default:
            timestampable: true
            sluggable: true
             dql:

apy_breadcrumb_trail:
    template: ::breadcrumb_trial.html.twig

knp_paginator:
    page_range:                 5          # default page range used in pagination control
    default_options:
        page_name:              page       # page query parameter name
        sort_field_name:        sort       # sort field query parameter name
        sort_direction_name:    direction  # sort direction query parameter name
        distinct:               true       # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig                         # sort link template

vich_uploader:
    db_driver: orm
    storage: file_system  # or gaufrette or flysystem
    metadata:
        auto_detection: true
        cache: file
    mappings:
        amenity_icon:
            uri_prefix: /uploads/amenity/icons
            upload_destination: '%kernel.project_dir%/web/uploads/amenity/icons'
            namer: vich_uploader.namer_origname
            delete_on_update:   true
            delete_on_remove:   true
        city_image:
            uri_prefix: /uploads/city
            upload_destination: '%kernel.project_dir%/web/uploads/city'
            namer: vich_uploader.namer_origname
            delete_on_update:   true
            delete_on_remove:   true
        images:
            uri_prefix: /uploads/images
            upload_destination: '%kernel.project_dir%/web/uploads/images'
            namer: vich_uploader.namer_origname
            delete_on_update:   true
            delete_on_remove:   true
        floorplan_image:
            uri_prefix: /uploads/floorplan/images
            upload_destination: '%kernel.project_dir%/web/uploads/floorplan/images'
            namer: vich_uploader.namer_origname
            delete_on_update:   true
            delete_on_remove:   true
        property_documents:
            uri_prefix: /uploads/property/documents
            upload_destination: '%kernel.project_dir%/web/uploads/property/documents'
            namer: vich_uploader.namer_origname
            delete_on_update:   true
            delete_on_remove:   true
        property_agreement_form:
            uri_prefix: /uploads/property/forms
            upload_destination: '%kernel.project_dir%/web/uploads/property/forms'
            namer: vich_uploader.namer_origname
            delete_on_update:   true
            delete_on_remove:   true

oneup_uploader:
    mappings:
        property_images:
            frontend: dropzone


# config/packages/sonata.yaml
sonata_block:
    default_contexts: [sonata_page_bundle]
    blocks:
        sonata.admin.block.admin_list:
            contexts:   [admin]

        #sonata.admin_doctrine_orm.block.audit:
        #    contexts:   [admin]

        sonata.block.service.text:
        sonata.block.service.rss:

        property.slider.block:
        property.requirement_post.block:
        property.top_rated_property.block:
        property.view_property.block:
        property.subscribe.block:
        property.featured_property.block:
        property.testimonial.block:
        property.news.block:
        property.trusted.block:

        # Some specific block from the SonataMediaBundle
        #sonata.media.block.media:
        #sonata.media.block.gallery:
        #sonata.media.block.feature_media:

        # Some block with different templates
        #acme.demo.block.demo:
        #    templates:
        #       - { name: 'Simple', template: '@AcmeDemo/Block/demo_simple.html.twig' }
        #       - { name: 'Big',    template: '@AcmeDemo/Block/demo_big.html.twig' }


# LiipImagineBundle
liip_imagine:
    cache: default
    resolvers:
        default:
            web_path:
                web_root: "%kernel.root_dir%/../web" # %kernel.root_dir%/../web
                cache_prefix: "media/cache" # media/cache

    filter_sets:
        cache: ~
        top_rated_property_large:
            quality: 100
            filters:
                thumbnail: { size: [263, 385], mode: outbound }
        top_rated_property_small:
            quality: 100
            filters:
                thumbnail: { size: [262, 180], mode: outbound }
        property_detail_slider:
            quality: 100
            filters:
                thumbnail: {size: [1140, 390], mode: outbound}
        property_detail_amenity_icon:
            quality: 100
            filters:
                thumbnail: {size: [36,36], mode: outbound}
        property_listing_image:
            quality: 100
            filters:
                thumbnail: {size: [158,186], mode: outbound}
        property_listing_gallery_image:
            quality: 100
            filters:
                thumbnail: {size: [550,245], mode: outbound}
        property_detail_document_image:
            quality: 100
            filters:
                thumbnail: {size: [505,300], mode: outbound}
